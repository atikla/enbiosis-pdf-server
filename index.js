require('dotenv').config();
const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const errorHandler = require('./handlers/error');
const i18n = require('./helpers/i18n');
const pdfRoutes = require('./routes/pdf');

const PORT = process.env.PORT || 4000;

app.use(cors());
app.use(i18n);
app.use(bodyParser.json({limit: '25mb'}));
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname + "/storage"));

// app.use(function(req, res, next) {
//     res.header("Access-Control-Allow-Origin", "https://enbiosis-client.herokuapp.com"); // update to match the domain you will make the request from
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     next();
// });

// ROUTES GO HERE
app.use('/api/pdf', pdfRoutes);

app.use((req, res, next) => {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use(errorHandler);

app.listen(PORT, () => {
    console.log(`Server is starting on port ${PORT}`);
});