const express = require('express');
const router = express.Router();
const {generatePDF, generateHealthPDF, generateNutritionProgram} = require('../handlers/pdf');

router.route('/generate')
    .post(generatePDF);

router.route('/generate-health')
    .post(generateHealthPDF);

router.route('/generate-nutrition-program')
    .post(generateNutritionProgram)

module.exports = router