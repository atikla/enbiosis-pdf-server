const axios = require('axios');
const FormData = require('form-data');
const fs = require('fs');
const puppeteer = require('puppeteer');
const {compile} = require('../helpers/pdf');
const i18n = require('i18n');
const {format} = require('date-fns');

// POST - /api/pdf/generate
exports.generatePDF = async (req, res, next) => {
    try{
        const lang = req.headers['content-language'];
        console.log(lang);
        i18n.setLocale(req, lang);
        (async () => {
            // launch the browser
            const browser = await puppeteer.launch({
                args: ['--no-sandbox', '--disable-setuid-sandbox', '--disable-web-security', '--font-render-hinting=none', '--enable-local-file-accesses', '--start-maximized'],  
                headless: true,
                defaultViewport: null
            })
            try{
                const {isBase64, token, shareEmail, kitCode, userName, registerDate, microTestResult, taxForPDF, closeProfiles, allScoresPDF, foodList, isSecret} = req.body;
                console.log(isBase64);
                console.log(isSecret === true);
                // open pages in the hedaless browser
                const microPage = await browser.newPage()
                const foodPage = await browser.newPage()
                // compile the template and its data
                const microContent = await compile('microbiome', {
                    req, 
                    kitCode, 
                    userName, 
                    registerDate, 
                    microTestResult, 
                    taxForPDF, 
                    closeProfiles, 
                    allScoresPDF, 
                    isHealth: false, 
                    isSecret: isSecret === true
                });
                const foodContent = await compile('food', {
                    browser, 
                    req, 
                    kitCode, 
                    userName, 
                    registerDate, 
                    allFoods: foodList.find(foods => foods.id === 'Tümü'), 
                    foodList: foodList.filter(foods => foods.id !== 'Tümü'), 
                    isSecret: isSecret === true
                })
                // pass the templates to the opened pages
                await microPage.setContent(microContent, {waitUntil: 'networkidle2'})
                await foodPage.setContent(foodContent, {waitUntil: 'networkidle2'})
                // await page.goto('data:text/html;charset=UTF-8,' + content, {waitUntil: 'networkidle2'});
                // await page.screenshot({ path: 'storage/output.png' });
                
                if(!isBase64){
                    // create the pdfs and store them in the storage directory
                    await microPage.pdf({path: `storage/user-pdf/microbiome_${kitCode}.pdf`, format: 'A4', printBackground: true, preferCSSPageSize: true})
                    await foodPage.pdf({path: `storage/user-pdf/food_${kitCode}.pdf`, format: 'A4', printBackground: true})
                    
                    // close the browser
                    await browser.close()
                    console.log('finished..............');

                    let formData = new FormData();
                    formData.append('email', shareEmail);
                    formData.append('foods', fs.createReadStream(`storage/user-pdf/food_${kitCode}.pdf`));
                    formData.append('microbiome', fs.createReadStream(`storage/user-pdf/microbiome_${kitCode}.pdf`));
                    formData.append('kit_code', kitCode);
                    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
                    getHeaders(formData)
                        .then(headers => {
                            return axios.post('https://www.enbiosis.com/api/user/kit/report', formData, {
                                headers: headers
                            }
                        )})
                        .then(response => {
                            console.log('response: ', response.data.message);
                            fs.unlinkSync(`storage/user-pdf/microbiome_${kitCode}.pdf`);
                            fs.unlinkSync(`storage/user-pdf/food_${kitCode}.pdf`);
                            return res.status(200).json({
                                message: response.data.message
                            })
                        })
                        .catch(err => {
                            console.log('err: ', err.response.data);
                            fs.unlinkSync(`storage/user-pdf/microbiome_${kitCode}.pdf`);
                            fs.unlinkSync(`storage/user-pdf/food_${kitCode}.pdf`);
                            return next({
                                status: 400,
                                message: err
                            });
                        })
                }else {
                    // create the pdfs and store them in the storage directory
                    const microBuffer = await microPage.pdf({format: 'A4', printBackground: true, preferCSSPageSize: true})
                    const foodBuffer = await foodPage.pdf({format: 'A4', printBackground: true})
                    
                    // close the browser
                    await browser.close();
                    console.log('finished..............');
                    
                    const microBase64 = `data:application/pdf;base64,${microBuffer.toString('base64')}`;
                    const foodBase64 = `data:application/pdf;base64,${foodBuffer.toString('base64')}`;

                    return res.status(200).json({
                        food: foodBase64, 
                        microbiome: microBase64
                    });
                }
            }catch(err){
                console.log('our err: ', err);
                await browser.close();
                return next({
                    status: 400,
                    message: err.message
                });
            }
        })()
    }catch(err){
        console.log(err);
        return next({
            status: 400,
            message: err.message
        });
    }
}

// POST - /api/pdf/generate-health
exports.generateHealthPDF = async (req, res, next) => {
    try{
        const lang = req.headers['content-language'];
        console.log(lang);
        i18n.setLocale(req, lang);
        (async () => {
            // launch the browser
            const browser = await puppeteer.launch({
                args: ['--no-sandbox', '--disable-setuid-sandbox', '--disable-web-security', '--font-render-hinting=none', '--enable-local-file-accesses', '--start-maximized'],  
                headless: true,
                defaultViewport: null
            })
            try{
                const {kitCode, userName, registerDate, microTestResult, taxForPDF, closeProfiles, allScoresPDF, doctorData, isSecret} = req.body;
                console.log(isSecret === true);
                // open pages in the hedaless browser
                const doctorPage = await browser.newPage()
                // compile the template and its data
                const doctorContent = await compile('microbiome', {
                    browser, 
                    req, 
                    kitCode, 
                    userName, 
                    registerDate, 
                    microTestResult, 
                    taxForPDF, 
                    closeProfiles, 
                    allScoresPDF, 
                    doctorData, 
                    isHealth: true, 
                    isSecret: isSecret === true
                });
                // pass the templates to the opened pages
                await doctorPage.setContent(doctorContent, {waitUntil: 'networkidle2'})
                // await page.goto('data:text/html;charset=UTF-8,' + content, {waitUntil: 'networkidle2'});
                // await page.screenshot({ path: 'storage/output.png' });


                // create the pdfs and store them in the storage directory
                const doctorBuffer = await doctorPage.pdf({format: 'A4', printBackground: true, preferCSSPageSize: true})
                
                // close the browser
                await browser.close()
                console.log('finished..............');
                
                const doctorBase64 = `data:application/pdf;base64,${doctorBuffer.toString('base64')}`;

                return res.status(200).json({
                    doctor: doctorBase64
                });
            }catch(err){
                console.log('our err: ', err);
                await browser.close();
                return next({
                    status: 400,
                    message: err.message
                });
            }
        })()
    }catch(err){
        console.log(err);
        return next({
            status: 400,
            message: err.message
        })
    }
}

// GET - /api/pdf/generate-nutrition-program
exports.generateNutritionProgram = async (req, res, next) => {
    try{
        const lang = req.headers['content-language'];
        console.log(lang);
        i18n.setLocale(req, lang);
        (async () => {
            // launch the browser
            const browser = await puppeteer.launch({
                args: ['--no-sandbox', '--disable-setuid-sandbox', '--disable-web-security', '--font-render-hinting=none', '--enable-local-file-accesses', '--start-maximized'],  
                headless: true,
                defaultViewport: null
            })
            try{
                const {kit_code, start_date, end_date, content} = req.body;
                // open pages in the hedaless browser
                const nutritionProgramPage = await browser.newPage()
                // compile the template and its data
                const nutritionProgramContent = await compile('nutritionProgram', {
                    browser,
                    req,
                    registerDate: format(new Date(), 'yyyy-MM-dd'),
                    kitCode: kit_code,
                    start_date,
                    end_date,
                    content
                })
                // pass the templates to the opened pages
                await nutritionProgramPage.setContent(nutritionProgramContent, {waitUntil: 'networkidle0'})

                // create the pdfs and store them in the storage directory
                // const pdf = await nutritionProgramPage.pdf({path: `storage/nProgram-pdf/nutrition_program_${req.body.program.kit_code}.pdf`, format: 'A4', printBackground: true, preferCSSPageSize: true})
                const programBuffer = await nutritionProgramPage.pdf({format: 'A4', printBackground: true, preferCSSPageSize: true})

                // close the browser
                await browser.close();
                console.log('finished..............');
                
                const programBase64 = `data:application/pdf;base64,${programBuffer.toString('base64')}`;

                res.status(200).json({
                    program: programBase64
                })
            }catch(err){
                console.log('our err: ', err);
                await browser.close();
                return next({
                    status: 400,
                    message: err.message
                });
            }
        })()
    }catch(err){
        console.log(err);
        return next({
            status: 400,
            message: err.message
        })
    }
}


function getHeaders(form) {
    return new Promise((resolve, reject) => {
        form.getLength((err, length) => {
            console.log('length: ', length);
            if(err) { reject(err); }
            let headers = Object.assign({'Content-Length': length}, form.getHeaders());
            resolve(headers);
        });
    });
}