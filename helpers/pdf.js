const promisedHandlebars = require('promised-handlebars');
const hbs = promisedHandlebars(require('handlebars'));
const fs = require('fs-extra');
const path = require('path');

const colors = {
    kotu: '#e3614d',
    orta: '#edd1a1',
    iyi: '#59c5d9'
}

const fills = {
    kotu: '#f9dfdb',
    orta: '#fbf5ec',
    iyi: '#def3f7'
}

const status_colors = {
    low: '#f7931e',
    mid: '#83c341',
    high: '#d0021b'
}

const gsTitles = ['Beden Kitle İndeksin', 'Karbonhidrat Metabolizman', 'Protein Metabolizman', 'Yağ Metabolizman', 'Vitamin Sentezin', 'Laktoz Hassasiyetin', 'Glüten Hassasiyetin', 'Şeker Tüketimin', 'Yapay Tatlandırıcı Hasarın', 'Probiyotik Organizmaların', 'Bağırsak Hareketliliğin', 'Antibiyotik Hasarın', 'Otoimmünite Endeksin', 'Uyku Kaliten']

exports.compile = async (templateName, data) => {
    const filePath = path.join(process.cwd(), 'templates', `${templateName}.hbs`)
    const html = await fs.readFile(filePath, 'utf-8');
    return hbs.compile(html)(data);
}

// translate helper
hbs.registerHelper('trans', (req, key) => {
    // console.log(req.getLocale());
    return req.__(key)
})

// MICROBIOME PDF HELPERS
hbs.registerHelper('printWorldSvgs', (isHealth, req, allScoresPDF) => {
    return gsTitles.map((title, index) => {
        const foundScore = allScoresPDF.find(score => title === score.id);
        const myScore = foundScore.scores.find(sc => sc.isMy);
        if(index % 2 === 0){
            return (
                `
                    <div class='w-item-wrapper w-end-position gs-item' style='top: ${index * 55}px'>
                        <div class='gs-rank  t-bold'>
                            ${isHealth ? req.__(foundScore.analizType) : ''}
                        </div>
                        <svg 
                            id="Layer_1" 
                            data-name="Layer 1" 
                            xmlns="http://www.w3.org/2000/svg" 
                            width='45' height='45'
                            viewBox="0 0 288.38 288.38"
                        >
                            ${getRankSvg(foundScore.analizType)}
                            <text 
                                style='font-size: 7rem;'
                                x='50%' 
                                y='65%' 
                                text-anchor='middle'
                                fill='white'
                            >
                                ${myScore.value}
                            </text>
                        </svg>
                        <div class='e-purple t-bold gs-title'>
                            ${allScoresPDF[index].head}
                        </div>
                        ${chooseGSSvg(title, '#4f4fa2', '#bfbfff', 60, 40)}
                    </div>
                `
            )
        }
        return (
            `
                <div class='w-item-wrapper gs-item' style='top: ${index * 55}px'>
                    ${chooseGSSvg(title, '#2597d3', '#bbe4f9', 60, 40)}
                    <div class='primary t-bold gs-title'>
                        ${allScoresPDF[index].head}
                    </div>
                    <svg 
                        id="Layer_1" 
                        data-name="Layer 1" 
                        xmlns="http://www.w3.org/2000/svg" 
                        width='45' height='45'
                        viewBox="0 0 288.38 288.38"
                    >
                        ${getRankSvg(foundScore.analizType)}
                        <text 
                            style='font-size: 7rem;'
                            x='50%' 
                            y='65%' 
                            text-anchor='middle'
                            fill='white'
                        >
                            ${myScore.value}
                        </text>
                    </svg>
                    <div class='gs-rank t-bold'>
                        ${isHealth ? req.__(foundScore.analizType) : ''}
                    </div>
                </div>
            `
        )
    }).join('');
})

hbs.registerHelper('printAgeValue', (isHealth, req, age) => 
    `
        ${isHealth ? `<div class='gs-rank t-bold'>(${req.__(age.rank)})</div>` : ''}
        <div class='t-bold a-text' style='color: ${colors[age.rank]}'>
            ${age.head}
        </div>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 174.28 156.11" width='60' height='60'>
            <g id="Layer_2" data-name="Layer 2">
            <g id="_1" data-name="1">
            <path fill=${fills[age.rank]} d="M121,0H53.25A19.34,19.34,0,0,0,36.49,9.68L2.59,68.38a19.38,19.38,0,0,0,0,19.35l33.9,58.71a19.33,19.33,0,0,0,16.76,9.67H121a19.34,19.34,0,0,0,16.76-9.67l33.89-58.71a19.33,19.33,0,0,0,0-19.35L137.79,9.68A19.35,19.35,0,0,0,121,0Z"/>
            </g>
            </g>
            <text x='50%' y='67%' class='mt-value t-bold' fill=${colors[age.rank]}>
                ${age.value.length === 1 ? `0${age.value}` : age.value}
            </text>
        </svg>
        
    `
)

hbs.registerHelper('printRangeValue', (isHealth, req, range) => 
    `
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 174.28 156.11" width='60' height='60'>
            <g id="Layer_2" data-name="Layer 2">
            <g id="_1" data-name="1">
            <path fill=${fills[range.rank]} d="M121,0H53.25A19.34,19.34,0,0,0,36.49,9.68L2.59,68.38a19.38,19.38,0,0,0,0,19.35l33.9,58.71a19.33,19.33,0,0,0,16.76,9.67H121a19.34,19.34,0,0,0,16.76-9.67l33.89-58.71a19.33,19.33,0,0,0,0-19.35L137.79,9.68A19.35,19.35,0,0,0,121,0Z"/>
            </g>
            </g>
            <text x='50%' y='67%' class='mt-value t-bold' fill='${colors[range.rank]}'>
                ${range.value}
            </text>
        </svg>
        <div class='t-bold r-text' style='color: ${colors[range.rank]}'>
            ${range.head}
        </div>
        ${isHealth ? `<div class='gs-rank t-bold'>(${req.__(range.rank)})</div>` : ''}
    `
)

hbs.registerHelper('printMicroAge', age => 
    `
        <div class='main-title t-bold t-margin' style='color: ${colors[age.rank]}'>
            ${age.head}
        </div>
        <div class='age-icon'>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 174.28 156.11" width='250' height='250'>
                <g id="Layer_2" data-name="Layer 2">
                <g id="_1" data-name="1">
                <path fill=${fills[age.rank]} d="M121,0H53.25A19.34,19.34,0,0,0,36.49,9.68L2.59,68.38a19.38,19.38,0,0,0,0,19.35l33.9,58.71a19.33,19.33,0,0,0,16.76,9.67H121a19.34,19.34,0,0,0,16.76-9.67l33.89-58.71a19.33,19.33,0,0,0,0-19.35L137.79,9.68A19.35,19.35,0,0,0,121,0Z"/>
                </g>
                </g>
                <text x='50%' y='67%' class='mt-value t-bold' fill=${colors[age.rank]}>
                    ${age.value.length === 1 ? `0${age.value}` : age.value}
                </text>
            </svg>
        </div>
        <div class='t-bold t-italic' style='color: ${colors[age.rank]}'>
            ${age.note}
        </div>
        ${
            age.desc.trim().split('.').map(d => 
                d !== '' ? `
                    <div class='t-margin'>
                        ${d}.
                    </div>
                ` : ''
            ).join('')
        }
    `
)

hbs.registerHelper('printMicroRange', range => 
    `
        <div class='main-title t-bold t-margin' style='color: ${colors[range.rank]}'>
            ${range.head}
        </div>
        <div class='age-icon'>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 174.28 156.11" width='250' height='250'>
                <g id="Layer_2" data-name="Layer 2">
                <g id="_1" data-name="1">
                <path fill=${fills[range.rank]} d="M121,0H53.25A19.34,19.34,0,0,0,36.49,9.68L2.59,68.38a19.38,19.38,0,0,0,0,19.35l33.9,58.71a19.33,19.33,0,0,0,16.76,9.67H121a19.34,19.34,0,0,0,16.76-9.67l33.89-58.71a19.33,19.33,0,0,0,0-19.35L137.79,9.68A19.35,19.35,0,0,0,121,0Z"/>
                </g>
                </g>
                <text x='50%' y='60%' class='mt-value t-bold' fill=${colors[range.rank]}>
                    ${range.value}
                </text>
                <text x='50%' y='85%' class='mt-text' fill=${colors[range.rank]}>
                    ${range.rank_title}
                </text>
            </svg>
        </div>
        <div class='t-bold t-italic' style='color: ${colors[range.rank]}'>
            ${range.note}
        </div>
        ${
            range.desc.trim().split('.').map(d => 
                d !== '' ? `
                    <div class='t-margin'>
                        ${d}.
                    </div>
                ` : ''
            ).join('')
        }
    `
)

hbs.registerHelper('printTaxResults', (req, isSecret, kitCode, registerDate, taxForPDF) => {
    return taxForPDF.map((tax, index) => {
        // refactoring
        const COLORS = tax.items[0].scores.length > 6 ? 
            ['#59c4d9', '#2699d6', '#edd1a1', '#e3614d', '#FFE7BF', '#f59178', '#4f4fa1', '#9696bb', '#e67475', '#DDCCFF'] : 
            ['#4f4fa1', '#59c4d9', '#fe7a6e', '#e67475', '#E8AEB0', '#DDCCFF', '#FFE7BF'];
        return (
            `
                <div id='tax-page-${index}' class='page portrait'>
                    <div class='page-header'>
                        <div class='report-name'>
                            <label class='t-bold secondary'>${req.__("p-head-l-0")}</label>
                            <div>${req.__("m-report-title")}</div>
                        </div>
                        <div class='report-kitCode'>
                            <label class='t-bold secondary'>${req.__("p-head-l-1")}</label>
                            <div>
                                ${isSecret ? '.....' : `${kitCode}`}
                            </div>
                        </div>
                        <div>
                            <label class='t-bold secondary'>${req.__("p-head-l-2")}</label>
                            <div>${registerDate}</div>
                        </div>
                    </div>
                    <hr class='line-breaker'>

                    <div class='t-bold primary'>
                        ${req.__("tax-title")}
                    </div>
                    <div class='alt-title t-margin t-bold primary'>
                        ${tax.title}
                    </div>
                    <div class='t-margin'>
                        ${tax.desc}
                    </div>
                    <div class='tbs-container'>
                        ${createTaxBars(tax.items, COLORS)}
                    </div>

                    <div class='page-footer'>
                            <img class='footer-img' src='https://enbiosis-pdf-generator.herokuapp.com/img/logo-2x.png' alt='enbiosis-logo'/>
                        </div>
                    </div>
                </div>
            `
        )
    }).join('');
})

hbs.registerHelper('printCloseProfiles', closeProfiles => 
    closeProfiles.map(close => 
        `
            <div class='close-wrapper'>
                <div class='close-title t-capita t-bold'>
                    ${close.title}
                </div>
                <div class='cpb-wrapper'>
                    <svg class='cpb'>
                        <circle cx='50%' cy='50%' r='55'></circle>
                        <circle cx='50%' cy='50%' r='55' style='stroke-dashoffset: calc(345 - (345 * ${close.value}) / 100)'></circle>
                    </svg>
                    <div class='t-bold primary close-value'>%${close.value}</div>
                </div>
            </div>
        `
    ).join('')
)

hbs.registerHelper('printAllScores', (req, isSecret, kitCode, registerDate, allScoresPDF) => {
    return allScoresPDF.map((score, index) => {
        const color = colors[score.analizType];
        const icon = getRankSvg(score.analizType);
        const colorGradient = score.colorScale.reduce((accumulator, currentValue, index, src) => {
            if(index === src.length - 1){
                return `${accumulator}${currentValue.color} ${currentValue.rate}%`
            }
            return `${accumulator}${currentValue.color} ${currentValue.rate}%, `
        }, '');
        
        const myScore = score.scores.filter(sc => sc.isMy)
            .map(sc => {
                const leftPostition = +sc.value < 3 ? 3 : +sc.value > 98 ? 98 : +sc.value;
                return (
                    `
                        <div class='score-bar-alt'>
                            <div class='s-container' style='left: calc(${leftPostition}% - 24px); bottom: 60px'>
                                <div class='s-type-wrapper'>
                                    <svg 
                                        id="Layer_1" 
                                        data-name="Layer 1" 
                                        xmlns="http://www.w3.org/2000/svg" 
                                        width='45' height='45'
                                        viewBox="0 0 288.38 288.38"
                                    >
                                        ${icon}
                                        <text 
                                            style='font-size: 7rem;'
                                            x='50%' 
                                            y='65%' 
                                            text-anchor='middle'
                                            fill='white'
                                        >
                                            ${sc.value}
                                        </text>
                                    </svg>
                                    <img class='arrow-icon my-img' src='https://enbiosis-pdf-generator.herokuapp.com/img/arrow-head.png' alt='arrow-icon'/>
                                </div>
                            </div>
                        </div>
                    `
                )
            }).join('');

        const otherScores = score.scores.filter(sc => !sc.isMy)
            .sort((a, b) => (a.value > b.value) ? 1 : -1)
            .map((sc, index, arr) => {
                let leftPosition = +sc.value > 98 ? 98 : +sc.value;
                if(index !== 0){
                    const difference = +sc.value - +arr[index - 1].value;
                    leftPosition = difference < 9 ? +sc.value + 2 : +sc.value;
                }

                return (
                    `
                        <div class='score-bar-alt'>
                            <div class='s-container s-display' style='left: calc(${leftPosition}% - 75px); top: 40px'>
                                <img class='arrow-icon' src='https://enbiosis-pdf-generator.herokuapp.com/img/arrow-head.png' alt='arrow-icon'/>
                                <div class='s-title-wrapper t-center legendary'>
                                    ${sc.title.split(' ').map(t => 
                                        `
                                            <div>
                                                ${t}
                                            </div>
                                        `
                                    ).join('')}
                                    <div class='t-bold'>
                                        ${sc.value}
                                    </div>
                                </div>
                            </div>
                        </div>
                    `
                )
            }).join('');
        return (
            `
                <div id='score-page-${index}' class='page portrait'>
                    <div class='page-header'>
                        <div class='report-name'>
                            <label class='t-bold secondary'>${req.__("p-head-l-0")}</label>
                            <div>${req.__("m-report-title")}</div>
                        </div>
                        <div class='report-kitCode'>
                            <label class='t-bold secondary'>${req.__("p-head-l-1")}</label>
                            <div>
                                ${isSecret ? '.....' : `${kitCode}`}
                            </div>
                        </div>
                        <div>
                            <label class='t-bold secondary'>${req.__("p-head-l-2")}</label>
                            <div>${registerDate}</div>
                        </div>
                    </div>
                    <hr class='line-breaker'>

                    <div class='head-wrapper'>
                        ${chooseGSSvg(score.id, color, fills[score.analizType], 225, 125)}
                        <div class='title-wrapper'>
                            <div class='t-bold' style='color: ${color}'>
                                ${req.__("gs-title")}
                            </div>
                            <div class='main-title t-bold' style='color: ${color}'>
                                ${score.head}
                            </div>
                        </div>
                    </div>
                    ${score.desc.map(d => 
                        d !== '' ? `
                            <div>
                                ${d}.
                            </div>
                        ` : ''
                    ).join('')}
                    
                    <div class='sb-wrapper'>
                        <hr class='sb-border'>
                        <div class='t-bold' style='color: ${color}'>
                            ${score.scores.find(score => score.isMy).title}:
                        </div>
                        <div class='score-bar-container'>
                            <div
                                class='score-bar'
                                style='background: linear-gradient(90deg, ${colorGradient})'
                            >
                                ${myScore}
                                ${otherScores}
                            </div>
                        </div>
                        <hr class='sb-border'>
                    </div>

                    <div class='page-footer'>
                        <img class='footer-img' src='https://enbiosis-pdf-generator.herokuapp.com/img/logo-2x.png' alt='enbiosis-logo'/>
                    </div>
                </div>
            `
        )
    }).join('');
})

const chunkArray = (arr, chunkItems) => (
    arr.reduce((acc, currentValue, index) => {
        const chunkIndex = Math.floor(index/chunkItems);

        if(!acc[chunkIndex]){ 
            acc[chunkIndex] = []
        }

        acc[chunkIndex].push(currentValue);

        return acc;
    }, [])
)


// FOOD PDF HELPERS
hbs.registerHelper('printFoodExamples', (req, allFoods) => {
    return allFoods.map(food => {
        let color = colors[food.rank];
        let sectionTitle = '';
        let icon = getRankSvg(food.rank);
        switch(food.rank){
            case 'iyi':
                sectionTitle = req.__("fex-title-0");
                break;
            case 'orta':
                sectionTitle = req.__("fex-title-1");
                break;
            case 'kotu':
                sectionTitle = req.__("fex-title-2");
                break;
            default:
                break;
        }
        return (
            `
                <div class='main-title t-bold t-margin' style='color: ${color};'>
                    ${sectionTitle}
                </div>
                <div class='ex-container t-margin'>
                    ${food.items.slice(0, 6).map(item => 
                        `
                            <div class='ex-wrapper'>
                                <div class='t-capita t-bold legendary'>
                                    ${item.foodTitle}
                                </div>
                                <svg 
                                    id="Layer_1" 
                                    data-name="Layer 1" 
                                    xmlns="http://www.w3.org/2000/svg" 
                                    width='35' height='35'
                                    viewBox="0 0 288.38 288.38"
                                >
                                    ${icon}
                                    <text 
                                        style='font-size: 8rem;'
                                        x='50%' 
                                        y='65%' 
                                        text-anchor='middle'
                                        fill='white'
                                    >
                                        ${item.foodValue}
                                    </text>
                                </svg>
                            </div>
                        `
                    ).join('')}
                </div>
            `
        )
    }).join('');
})

hbs.registerHelper('printAllFoods', (req, isSecret, kitCode, registerDate, foodList) => {
    let viewList = foodList.map(foods => {
        let itemsList = foods.foodItems.map(list => {
            let icon = getRankSvg(list.rank);
            return list.items.map(item => 
                `
                    <div class='f-card'>
                        <img class='f-img' src='${item.imageUrl}' title='${item.altTitle}'/>
                        <div class='t-capita t-bold legendary'>
                            ${item.foodTitle}
                        </div>
                        <svg 
                            id="Layer_1" 
                            data-name="Layer 1" 
                            xmlns="http://www.w3.org/2000/svg" 
                            width='40' height='40'
                            viewBox="0 0 288.38 288.38"
                        >
                            ${icon}
                            <text 
                                style='font-size: 8rem;'
                                x='50%' 
                                y='65%' 
                                text-anchor='middle'
                                fill='white'
                            >
                                ${item.foodValue}
                            </text>
                        </svg>
                    </div>
                `    
            )
        })
        itemsList = itemsList.reduce((acc, currentValue) => {
            return [...acc, ...currentValue]
        }, [])
        if(itemsList.length > 18){
            return ([
                `
                    <div class='alt-title t-margin t-bold primary'>
                        ${foods.foodType}
                    </div>
                    <div class='f-card-container'>
                        ${itemsList.slice(0, 18).join('')}
                    </div>
                `, 
                `
                    <div class='alt-title t-margin t-bold primary'>
                        ${foods.foodType}
                    </div>
                    <div class='f-card-container'>
                        ${itemsList.slice(18).join('')}
                    </div>
                `
            ])
        }
        return (
            `
                <div class='alt-title t-margin t-bold primary'>
                    ${foods.foodType}
                </div>
                <div class='f-card-container'>
                    ${itemsList.join('')}
                </div>
            `
        )
    }).flat();

    const firstViewList = viewList.slice(0, viewList.length - 2).map((view, index) => 
        `
            <div id='food-page-${index}' class='page portrait'>
                <div class='page-header'>
                    <div class='report-name'>
                        <label class='t-bold secondary'>${req.__("p-head-l-0")}</label>
                        <div>${req.__("f-report-title")}</div>
                    </div>
                    <div class='report-kitCode'>
                        <label class='t-bold secondary'>${req.__("p-head-l-1")}</label>
                        <div>
                            ${isSecret ? '.....' : `${kitCode}`}
                        </div>
                    </div>
                    <div>
                        <label class='t-bold secondary'>${req.__("p-head-l-2")}</label>
                        <div>${registerDate}</div>
                    </div>
                </div>
                <hr class='line-breaker'>
                
                ${index === 0 ? 
                    `<div class='main-title t-bold primary'>
                        ${req.__("fs-title")}
                    </div>` : 
                    ''
                }
                ${view}
                <div class='page-footer'>
                    <img class='footer-img' src='https://enbiosis-pdf-generator.herokuapp.com/img/logo-2x.png' alt='enbiosis-logo'/>
                </div>
            </div>
        `
    ).join('');

    const secondViewList = `
        <div id='food-page-${viewList.length - 2}' class='page portrait'>
            <div class='page-header'>
                <div class='report-name'>
                    <label class='t-bold secondary'>${req.__("p-head-l-0")}</label>
                    <div>${req.__("f-report-title")}</div>
                </div>
                <div class='report-kitCode'>
                    <label class='t-bold secondary'>${req.__("p-head-l-1")}</label>
                    <div>
                        ${isSecret ? '.....' : `${kitCode}`}
                    </div>
                </div>
                <div>
                    <label class='t-bold secondary'>${req.__("p-head-l-2")}</label>
                    <div>${registerDate}</div>
                </div>
            </div>
            <hr class='line-breaker'>
            
            ${viewList.slice(viewList.length - 2).join('')}
            
            <div class='page-footer'>
                <img class='footer-img' src='https://enbiosis-pdf-generator.herokuapp.com/img/logo-2x.png' alt='enbiosis-logo'/>
            </div>
        </div>
    `
    return `${firstViewList}${secondViewList}`;
})

// function for build the taxonomic result bars and legends
const createTaxBars = (items, colors) => {
    return items.map(item => {
        let scoreWidth = 0;
        const fixedWidth = 51 * item.scores.length;
        const scoresBar = item.scores.map((score, index, arr) => {
            scoreWidth += (51 + ((816 - fixedWidth) * (818 * +score.value.toFixed(1) / 100) / 818))
            return (`
                <div class='ts-container' style='
                    width: ${scoreWidth}px; 
                    background-color: ${colors[index]}; 
                    z-index: ${arr.length - index}; 
                    -webkit-box-shadow: ${index !== arr.length - 1 ? '7px 0px 5px -5px rgba(0,0,0,0.2)' : 'none'};
                    -moz-box-shadow: ${index !== arr.length - 1 ? '7px 0px 5px -5px rgba(0,0,0,0.2)' : 'none'};
                    box-shadow: ${index !== arr.length - 1 ? '7px 0px 5px -5px rgba(0,0,0,0.2)' : 'none'};'
                >
                    <div class='ts-value'>
                        %${+score.value.toFixed(1)}
                    </div>
                </div>
            `)
        }).join('');
        const scoreLegends = item.scores.map((score, index) => 
            `
                <div class='legend-wrapper'>
                    <div class='legend-dot' style='background: ${colors[index]}'></div>
                    <div class='t-bold'>${score.name}</div>
                </div>
            `
        ).join('');
        return (
            `
                <div style='
                    margin-bottom: ${item.isMy ? '150px' : '60px'}
                '>
                    <div class='t-margin t-bold' style='font-size: 16pt'>
                        ${item.head}
                    </div>
                    <div class='tax-container t-margin'>
                        ${scoresBar}
                    </div>
                    <div class='legends-container'>
                        ${scoreLegends}
                    </div>
                </div>
            `
        )
    }).join('');
}

// function for choosing the svg path for the rank icon
const getRankSvg = rank => {
    switch(rank){
        case 'iyi':
            return (
                `
                    <path 
                        fill='#59c5d9'
                        d="M199.6,288.38H88.78a13,13,0,0,1-13-13V13a13,13,0,0,1,13-13H199.6a13,13,0,0,1,13,13V275.35a13,13,0,0,1-13,13"
                    />
                    <path 
                        fill='#59c5d9'
                        d="M0,199.6V88.78a13,13,0,0,1,13-13H275.35a13,13,0,0,1,13,13V199.6a13,13,0,0,1-13,13H13a13,13,0,0,1-13-13"
                    />
                `
            )
        case 'orta':
            return (
                `
                    <path 
                        fill='#edd1a1'
                        d="M86.68,264.2,23.73,201.25a81,81,0,0,1,0-114.57l63-62.95a81,81,0,0,1,114.57,0l62.95,63a81,81,0,0,1,0,114.57L201.25,264.2a81,81,0,0,1-114.57,0"
                    />
                `
            )
        case 'kotu':
            return (
                `
                    <path 
                        fill='#e3614d'
                        d="M82.17,286.09,3.82,207.73a13,13,0,0,1,0-18.43L189.3,3.82a13,13,0,0,1,18.43,0l78.36,78.35a13,13,0,0,1,0,18.43L100.6,286.09a13,13,0,0,1-18.43,0"
                    />
                    <path 
                        fill='#e3614d'
                        d="M3.82,82.17,82.17,3.82a13,13,0,0,1,18.43,0L286.09,189.3a13,13,0,0,1,0,18.43l-78.36,78.36a13,13,0,0,1-18.43,0L3.82,100.6a13,13,0,0,1,0-18.43"
                    />
                `
            )
        default:
            return '';
    }
}

// fucntion for making a dynamic gs svg with the passed colors and sizes
const chooseGSSvg = (head, pathColor, hexColor, hxSize, gsSize) => {
    switch(head){
        case 'Uyku Kaliten':
            return (`
                <div class='gs-icon-container'>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 174.28 156.11" width='${hxSize}' height='${hxSize}'>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="_1" data-name="1">
                        <path fill=${hexColor} d="M121,0H53.25A19.34,19.34,0,0,0,36.49,9.68L2.59,68.38a19.38,19.38,0,0,0,0,19.35l33.9,58.71a19.33,19.33,0,0,0,16.76,9.67H121a19.34,19.34,0,0,0,16.76-9.67l33.89-58.71a19.33,19.33,0,0,0,0-19.35L137.79,9.68A19.35,19.35,0,0,0,121,0Z"/>
                        </g>
                        </g>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 77.47 50.98" width='${gsSize}' height='${gsSize}' style='position: absolute; left: calc(50% - ${gsSize/2}px); top: calc(50% - ${gsSize/2}px)'>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="_1" data-name="1">
                        <path fill=${pathColor} d="M77.47,35.42V48.67a2.31,2.31,0,1,1-4.62,0V37.73H4.62V48.67a2.31,2.31,0,0,1-4.62,0V2.31a2.31,2.31,0,0,1,4.62,0v30.8H75.16A2.32,2.32,0,0,1,77.47,35.42Z"/>
                        <path fill=${pathColor} d="M65.23,9.93H42.05a8.94,8.94,0,1,0,0,17.87H65.23a8.94,8.94,0,0,0,0-17.87Zm0,13.25H42.05a4.31,4.31,0,1,1,0-8.62H65.23a4.31,4.31,0,1,1,0,8.62Z"/>
                        <path fill=${pathColor} d="M18.87,9.93a8.94,8.94,0,1,0,8.93,8.94A8.95,8.95,0,0,0,18.87,9.93Zm0,13.25a4.31,4.31,0,1,1,4.31-4.31A4.32,4.32,0,0,1,18.87,23.18Z"/>
                        </g>
                        </g>
                    </svg>
                </div>
            `);
        case 'Yapay Tatlandırıcı Hasarın':
            return (`
                <div class='gs-icon-container'>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 174.28 156.11" width='${hxSize}' height='${hxSize}'>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="_1" data-name="1">
                        <path fill=${hexColor} d="M121,0H53.25A19.34,19.34,0,0,0,36.49,9.68L2.59,68.38a19.38,19.38,0,0,0,0,19.35l33.9,58.71a19.33,19.33,0,0,0,16.76,9.67H121a19.34,19.34,0,0,0,16.76-9.67l33.89-58.71a19.33,19.33,0,0,0,0-19.35L137.79,9.68A19.35,19.35,0,0,0,121,0Z"/>
                        </g>
                        </g>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 73.93 79.21" width='${gsSize}' height='${gsSize}' style='position: absolute; left: calc(50% - ${gsSize/2}px); top: calc(50% - ${gsSize/2}px)'>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="_1" data-name="1">
                        <path fill=${pathColor} d="M37,31.68a1.33,1.33,0,0,0,1.32-1.32V25.08A1.33,1.33,0,0,0,37,23.76H35.65v-4a1.33,1.33,0,0,0-1.32-1.32h-29A1.32,1.32,0,0,0,4,19.8v4H2.64a1.32,1.32,0,0,0-1.32,1.32v5.28a1.32,1.32,0,0,0,1.32,1.32H5.28v3.57L2.86,38.87a1.39,1.39,0,0,0-.22.74v33a6.6,6.6,0,0,0,6.6,6.6H30.36A6.61,6.61,0,0,0,37,72.61v-33a1.32,1.32,0,0,0-.23-.74l-2.41-3.62V31.68ZM4,26.4H15.84V23.76H6.6V21.12H33v2.64H27.72V26.4h7.93V29H4ZM7.31,37h25l2,3V62a20.06,20.06,0,0,1-6.93-1.8c-.67-.27-1.29-.53-1.9-.73-1.19-.4-2.16-.81-3-1.17a14.94,14.94,0,0,0-6.64-1.54A45.35,45.35,0,0,0,5.28,58.93V40Zm23.05,39.6H9.24a4,4,0,0,1-4-4V61.69c2.14-.67,7.61-2.28,10.56-2.28a12.46,12.46,0,0,1,5.62,1.33c.9.38,1.92.81,3.21,1.24.55.19,1.12.42,1.72.67a22.5,22.5,0,0,0,7.94,2v8A4,4,0,0,1,30.36,76.57Zm1.32-42.24H7.92V31.68H31.68Z"/>
                        <rect fill=${pathColor} x="18.48" y="23.76" width="2.64" height="2.64"/>
                        <path fill=${pathColor} d="M47.75,67.91,39.92,77a1.32,1.32,0,0,0,1,2.18H72.61a1.32,1.32,0,0,0,1-2.18L65.79,67.9A11.88,11.88,0,0,0,49,66.62a10.4,10.4,0,0,0-1.29,1.28Zm9-1.51a9.2,9.2,0,0,1,7,3.22l6,6.95H43.8l6-6.95A9.24,9.24,0,0,1,56.77,66.4Z"/>
                        <rect fill=${pathColor} x="51.49" y="71.29" width="2.64" height="2.64"/>
                        <rect fill=${pathColor} x="56.77" y="68.65" width="2.64" height="2.64"/>
                        <rect fill=${pathColor} x="56.77" y="43.57" width="2.64" height="2.64"/>
                        <rect fill=${pathColor} x="51.49" y="35.65" width="2.64" height="2.64"/>
                        <rect fill=${pathColor} x="58.09" y="26.4" width="2.64" height="2.64"/>
                        <rect fill=${pathColor} x="50.17" y="21.12" width="2.64" height="2.64"/>
                        <rect fill=${pathColor} x="62.05" y="56.77" width="2.64" height="2.64"/>
                        <rect fill=${pathColor} x="48.85" y="47.53" width="2.64" height="2.64"/>
                        <rect fill=${pathColor} x="48.85" y="58.09" width="2.64" height="2.64"/>
                        <path fill=${pathColor} d="M56.77,0C52.16,0,48,2.09,46,5.28H1.32A1.32,1.32,0,0,0,0,6.6v5.28A1.32,1.32,0,0,0,1.32,13.2H46c1.94,3.19,6.13,5.28,10.74,5.28,6.55,0,11.88-4.14,11.88-9.24S63.32,0,56.77,0Zm0,15.84c-4,0-7.48-1.82-8.76-4.52a1.3,1.3,0,0,0-1.18-.76H2.64V7.92H46.82A1.32,1.32,0,0,0,48,7.16c1.28-2.7,4.8-4.52,8.76-4.52,5.09,0,9.24,3,9.24,6.6S61.86,15.84,56.77,15.84Z"/>
                        <path fill=${pathColor} d="M50.31,8.65l2.35,1.2A4.47,4.47,0,0,1,55.59,8a1.36,1.36,0,0,0,1.18-1.31h0a1.31,1.31,0,0,0-1.46-1.32A7,7,0,0,0,50.31,8.65Z"/>
                        <rect fill=${pathColor} x="9.24" y="63.37" width="2.64" height="2.64"/>
                        <rect fill=${pathColor} x="14.52" y="66.01" width="2.64" height="2.64"/>
                        <rect fill=${pathColor} x="27.72" y="69.97" width="2.64" height="2.64"/>
                        <rect fill=${pathColor} x="22.44" y="71.29" width="2.64" height="2.64"/>
                        </g>
                        </g>
                    </svg>
                </div>
            `);
        case 'Otoimmünite Endeksin':
            return (`
                <div class='gs-icon-container'>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 174.28 156.11" width='${hxSize}' height='${hxSize}'>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="_1" data-name="1">
                        <path fill=${hexColor} d="M121,0H53.25A19.34,19.34,0,0,0,36.49,9.68L2.59,68.38a19.38,19.38,0,0,0,0,19.35l33.9,58.71a19.33,19.33,0,0,0,16.76,9.67H121a19.34,19.34,0,0,0,16.76-9.67l33.89-58.71a19.33,19.33,0,0,0,0-19.35L137.79,9.68A19.35,19.35,0,0,0,121,0Z"/>
                        </g>
                        </g>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 61.96 75.73" width='${gsSize}' height='${gsSize}' style='position: absolute; left: calc(50% - ${gsSize/2}px); top: calc(50% - ${gsSize/2}px)'>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="_1" data-name="1">
                        <path fill=${pathColor} d="M29.73,75.4C17.8,67.66,9.82,59.63,5.14,50S-.87,29.16.53,15.12a2.25,2.25,0,0,1,2-2C14,11.59,21.07,8.94,29.38.66a2.27,2.27,0,0,1,3.2,0c8.3,8.28,15.36,10.93,26.82,12.42a2.25,2.25,0,0,1,2,2c1.4,14,.07,25.22-4.61,34.86S44.15,67.66,32.23,75.4A2.53,2.53,0,0,1,29.73,75.4ZM31,3.22C22.5,11.52,15,14.38,3.41,15.94c-1.26,13.23,0,23.75,4.38,32.76S19.71,65.33,31,72.7c11.27-7.37,18.8-15,23.19-24s5.64-19.53,4.38-32.76C46.93,14.38,39.45,11.52,31,3.22Z" transform="translate(0 0)"/>
                        <path fill=${pathColor} d="M31,66.76a82.34,82.34,0,0,0,8.77-7c1.77-1.63,3.62.72,2.07,2.1a80,80,0,0,1-9.32,7.45,2.39,2.39,0,0,1-3.09,0C20.07,62.77,13.65,56.07,9.73,48.15S4.38,30.94,5.11,19.49a2.41,2.41,0,0,1,2.25-2.16C15.9,15.83,22.89,13,29.53,7.24a2.25,2.25,0,0,1,2.93,0C39.33,13.19,46.09,15.89,55,17.4a2.28,2.28,0,0,1,1.89,2.1c.56,8.82-.14,16.31-2.31,23-.72,2.25-3.53,1.29-2.82-.84,2-6.16,2.69-13.15,2.22-21.4C45,18.62,38,15.8,31,9.88c-7,5.91-14,8.74-23,10.33-.62,10.68.7,19.24,4.36,26.63S22.11,60.58,31,66.76Z" transform="translate(0 0)"/>
                        </g>
                        </g>
                    </svg>
                </div>
            `);
        case 'Beden Kitle İndeksin':
            return (`
                <div class='gs-icon-container'>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 174.28 156.11" width='${hxSize}' height='${hxSize}'>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="_1" data-name="1">
                        <path fill=${hexColor} d="M121,0H53.25A19.34,19.34,0,0,0,36.49,9.68L2.59,68.38a19.38,19.38,0,0,0,0,19.35l33.9,58.71a19.33,19.33,0,0,0,16.76,9.67H121a19.34,19.34,0,0,0,16.76-9.67l33.89-58.71a19.33,19.33,0,0,0,0-19.35L137.79,9.68A19.35,19.35,0,0,0,121,0Z"/>
                        </g>
                        </g>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 89.84 74.05" width='${gsSize}' height='${gsSize}' style='position: absolute; left: calc(50% - ${gsSize/2}px); top: calc(50% - ${gsSize/2}px)'>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="_1" data-name="1">
                        <path fill=${pathColor} d="M80.63,0H25.4a9.21,9.21,0,0,0-9.2,9.21V64.44a9.2,9.2,0,0,0,9.2,9.2H80.63a9.21,9.21,0,0,0,9.21-9.2V9.21A9.22,9.22,0,0,0,80.63,0Zm6.58,64.44A6.58,6.58,0,0,1,80.63,71H25.4a6.57,6.57,0,0,1-6.57-6.57V9.21A6.58,6.58,0,0,1,25.4,2.63H80.63a6.59,6.59,0,0,1,6.58,6.58ZM81.14,26.3H24.9a3.44,3.44,0,0,0-3.44,3.44v35.2a3.44,3.44,0,0,0,3.44,3.44H81.14a3.44,3.44,0,0,0,3.44-3.44V29.74A3.44,3.44,0,0,0,81.14,26.3Zm-57,38.64V29.74a.81.81,0,0,1,.81-.81H51.7V65.75H24.9A.81.81,0,0,1,24.09,64.94Zm57.86,0a.81.81,0,0,1-.81.81H54.33V28.93H81.14a.81.81,0,0,1,.81.81ZM37.24,23.67H68.8a1.31,1.31,0,0,0,1.31-1.31,17.1,17.1,0,1,0-34.19,0A1.32,1.32,0,0,0,37.24,23.67ZM53,7.89A14.49,14.49,0,0,1,67.43,21H53.56l-7.05-7a1.32,1.32,0,1,0-1.89,1.83l0,0L49.84,21H38.61A14.49,14.49,0,0,1,53,7.89Z"/>
                        <path fill=${pathColor} d="M4.61,74.05a1.23,1.23,0,0,1-1.23-1.23V1.65a1.24,1.24,0,1,1,2.47,0V72.82A1.24,1.24,0,0,1,4.61,74.05Z"/>
                        <path fill=${pathColor} d="M8,2.89H1.24a1.24,1.24,0,0,1,0-2.48H8A1.24,1.24,0,0,1,8,2.89Z"/>
                        <path fill=${pathColor} d="M8,74.05H1.24a1.24,1.24,0,1,1,0-2.47H8a1.24,1.24,0,0,1,0,2.47Z"/>
                        <path fill=${pathColor} d="M8,38.47H1.24a1.24,1.24,0,1,1,0-2.47H8a1.24,1.24,0,0,1,0,2.47Z"/>
                        <path fill=${pathColor} d="M6.34,20.68H2.89a1.24,1.24,0,0,1,0-2.48H6.34a1.24,1.24,0,0,1,0,2.48Z"/>
                        <path fill=${pathColor} d="M6.34,57.66H2.89a1.24,1.24,0,1,1,0-2.48H6.34a1.24,1.24,0,0,1,0,2.48Z"/>
                        </g>
                        </g>
                    </svg>
                </div>
            `);
        case 'Karbonhidrat Metabolizman':
            return (`
                <div class='gs-icon-container'>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 174.28 156.11" width='${hxSize}' height='${hxSize}'>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="_1" data-name="1">
                        <path fill=${hexColor} d="M121,0H53.25A19.34,19.34,0,0,0,36.49,9.68L2.59,68.38a19.38,19.38,0,0,0,0,19.35l33.9,58.71a19.33,19.33,0,0,0,16.76,9.67H121a19.34,19.34,0,0,0,16.76-9.67l33.89-58.71a19.33,19.33,0,0,0,0-19.35L137.79,9.68A19.35,19.35,0,0,0,121,0Z"/>
                        </g>
                        </g>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 84.19 65.55" width='${gsSize}' height='${gsSize}' style='position: absolute; left: calc(50% - ${gsSize/2}px); top: calc(50% - ${gsSize/2}px)'>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="_1" data-name="1">
                        <path fill=${pathColor} d="M69.89,65.55a2.29,2.29,0,0,1-.78-.14L58,61.41a2.29,2.29,0,0,1-1.38-2.93l4-11.21a2.3,2.3,0,0,1,2.94-1.38,2.27,2.27,0,0,1,1.3,1.19l1.52,3.24A25.12,25.12,0,0,0,64.17,9.66,2.28,2.28,0,0,1,63.25,7h0l1.36-3.67A2.31,2.31,0,0,1,68,2.1a33.56,33.56,0,0,1,2.1,56.09L72,62.28a2.31,2.31,0,0,1-2.08,3.27ZM59.39,59,69,62.41l-2.43-5.2,1-.66A30.77,30.77,0,0,0,67,4.79L66,7.54a27.9,27.9,0,0,1,.65,46.06l-1.35,1-2.45-5.25Z"/>
                        <path fill=${pathColor} d="M17.57,63.86a2.29,2.29,0,0,1-1.17-.32A33.55,33.55,0,0,1,14.13,7.35l-1.9-4.08A2.3,2.3,0,0,1,13.34.21,2.36,2.36,0,0,1,15.09.13l11.1,4a2.3,2.3,0,0,1,1.39,2.93h0l-4,11.19a2.3,2.3,0,0,1-4.25.19L17.8,15.22a25.07,25.07,0,0,0,2.31,40.67,2.26,2.26,0,0,1,.94,2.68l-1.3,3.75a2.31,2.31,0,0,1-2.18,1.55ZM15.22,3.13l2.43,5.2-1,.66a30.77,30.77,0,0,0,.67,51.86l1-2.83a27.84,27.84,0,0,1-.77-46.08L18.89,11l2.44,5.24L24.8,6.58Z"/>
                        <path fill=${pathColor} d="M60.42,43.36H25.62a9.2,9.2,0,0,1-9.29-9c0-5.46,3.56-10.42,9.81-13.77h0l.51-.27A33.3,33.3,0,0,1,36.8,17.15,38.16,38.16,0,0,1,43,16.67h.68a38,38,0,0,1,8.16.95H52a36.47,36.47,0,0,1,4.1,1.23,29.11,29.11,0,0,1,5.46,2.68c5.32,3.39,8.18,7.91,8.18,12.77v.09A9.21,9.21,0,0,1,60.42,43.36ZM27.92,21.85l-.4.21c-5.9,3-9.29,7.47-9.29,12.24a7.29,7.29,0,0,0,7.39,7.15h34.8a7.28,7.28,0,0,0,7.39-7.07V34.3c0-4.18-2.59-8.13-7.29-11.11a27,27,0,0,0-5.1-2.49,31.77,31.77,0,0,0-3.82-1.17h-.08a37.42,37.42,0,0,0-7.89-1H43a36.85,36.85,0,0,0-5.93.47A31.45,31.45,0,0,0,27.92,21.85Z"/>
                        <path fill=${pathColor} d="M25.52,43.57a9.46,9.46,0,0,1-9.41-9.16c0-5.5,3.62-10.59,9.93-14l.51-.28a33.48,33.48,0,0,1,10.22-3.23,39.57,39.57,0,0,1,6.16-.49h.77a39.09,39.09,0,0,1,8.21,1H52a35.43,35.43,0,0,1,4.18,1.24,29.14,29.14,0,0,1,5.49,2.7c5.34,3.41,8.28,8,8.28,12.95v.09a9.45,9.45,0,0,1-9.41,9.18h-35ZM42.93,16.88a38.89,38.89,0,0,0-6.1.48,33,33,0,0,0-10.08,3.19l-.51.27c-6.17,3.3-9.7,8.25-9.7,13.58a9,9,0,0,0,9,8.75h35a9,9,0,0,0,9-8.77V34.3c0-4.79-2.87-9.27-8.08-12.59A28.6,28.6,0,0,0,56,19.05a34.55,34.55,0,0,0-4.07-1.22h-.08a39.08,39.08,0,0,0-8.16-.95h-.77Zm17.6,24.79h-35A7.53,7.53,0,0,1,18,34.31c0-4.87,3.43-9.4,9.4-12.44l.41-.21a31.77,31.77,0,0,1,9.23-2.83,38.51,38.51,0,0,1,6-.47h.61a37.71,37.71,0,0,1,7.93,1h0a33.84,33.84,0,0,1,3.89,1.19A27.47,27.47,0,0,1,60.63,23C65.4,26,68,30.05,68,34.3v.08A7.52,7.52,0,0,1,60.53,41.67Zm-.11-.43h.11a7.1,7.1,0,0,0,7.07-6.87V34.3c0-4.1-2.56-8-7.2-10.93a26.33,26.33,0,0,0-5.06-2.47,36.05,36.05,0,0,0-3.79-1.17h0a37.15,37.15,0,0,0-7.89-.95H43a35.42,35.42,0,0,0-5.89.46A31.15,31.15,0,0,0,28,22l-.4.21c-5.83,3-9.17,7.36-9.17,12a7.09,7.09,0,0,0,7.08,6.94H60.42Z"/>
                        <path fill=${pathColor} d="M60.42,43.36H25.62a9.2,9.2,0,0,1-9.29-9c0-5.46,3.56-10.42,9.81-13.77h0l.51-.27A33.3,33.3,0,0,1,36.8,17.15,38.16,38.16,0,0,1,43,16.67h.68a38,38,0,0,1,8.16.95H52a36.47,36.47,0,0,1,4.1,1.23,29.11,29.11,0,0,1,5.46,2.68c5.32,3.39,8.18,7.91,8.18,12.77v.09A9.21,9.21,0,0,1,60.42,43.36ZM27.92,21.85l-.4.21c-5.9,3-9.29,7.47-9.29,12.24a7.29,7.29,0,0,0,7.39,7.15h34.8a7.28,7.28,0,0,0,7.39-7.07V34.3c0-4.18-2.59-8.13-7.29-11.11a27,27,0,0,0-5.1-2.49,31.77,31.77,0,0,0-3.82-1.17h-.08a37.42,37.42,0,0,0-7.89-1H43a36.85,36.85,0,0,0-5.93.47A31.45,31.45,0,0,0,27.92,21.85Z"/>
                        <path fill=${pathColor} d="M25.52,43.57a9.46,9.46,0,0,1-9.41-9.16c0-5.5,3.62-10.59,9.93-14l.51-.28a33.48,33.48,0,0,1,10.22-3.23,39.57,39.57,0,0,1,6.16-.49h.77a39.09,39.09,0,0,1,8.21,1H52a35.43,35.43,0,0,1,4.18,1.24,29.14,29.14,0,0,1,5.49,2.7c5.34,3.41,8.28,8,8.28,12.95v.09a9.45,9.45,0,0,1-9.41,9.18h-35ZM42.93,16.88a38.89,38.89,0,0,0-6.1.48,33,33,0,0,0-10.08,3.19l-.51.27c-6.17,3.3-9.7,8.25-9.7,13.58a9,9,0,0,0,9,8.75h35a9,9,0,0,0,9-8.77V34.3c0-4.79-2.87-9.27-8.08-12.59A28.6,28.6,0,0,0,56,19.05a34.55,34.55,0,0,0-4.07-1.22h-.08a39.08,39.08,0,0,0-8.16-.95h-.77Zm17.6,24.79h-35A7.53,7.53,0,0,1,18,34.31c0-4.87,3.43-9.4,9.4-12.44l.41-.21a31.77,31.77,0,0,1,9.23-2.83,38.51,38.51,0,0,1,6-.47h.61a37.71,37.71,0,0,1,7.93,1h0a33.84,33.84,0,0,1,3.89,1.19A27.47,27.47,0,0,1,60.63,23C65.4,26,68,30.05,68,34.3v.08A7.52,7.52,0,0,1,60.53,41.67Zm-.11-.43h.11a7.1,7.1,0,0,0,7.07-6.87V34.3c0-4.1-2.56-8-7.2-10.93a26.33,26.33,0,0,0-5.06-2.47,36.05,36.05,0,0,0-3.79-1.17h0a37.15,37.15,0,0,0-7.89-.95H43a35.42,35.42,0,0,0-5.89.46A31.15,31.15,0,0,0,28,22l-.4.21c-5.83,3-9.17,7.36-9.17,12a7.09,7.09,0,0,0,7.08,6.94H60.42Z"/>
                        <path fill=${pathColor} d="M39.35,31a2.87,2.87,0,0,1-2.89-2.83,3.15,3.15,0,0,1,0-.44A16.33,16.33,0,0,1,43.1,16.86a.83.83,0,0,1,.57-.18,38,38,0,0,1,8.3.95,1,1,0,0,1-.15,1.88,10.48,10.48,0,0,0-9.64,8.9A2.86,2.86,0,0,1,39.35,31ZM44,18.57a14.45,14.45,0,0,0-5.57,9.37,1,1,0,0,0,1,1.1,1,1,0,0,0,1-.89,12.45,12.45,0,0,1,3-6.35,12.74,12.74,0,0,1,4-3C46.18,18.69,45.07,18.57,44,18.57Z"/>
                        <path fill=${pathColor} d="M39.32,31.18a3.07,3.07,0,0,1-3.07-3,3.59,3.59,0,0,1,0-.47,16.71,16.71,0,0,1,6.69-11,1.11,1.11,0,0,1,.71-.22,39,39,0,0,1,8.34,1,1.17,1.17,0,0,1-.18,2.3,10.23,10.23,0,0,0-9.45,8.72,3.07,3.07,0,0,1-3,2.74Zm4.32-14.29a.71.71,0,0,0-.41.13A16.28,16.28,0,0,0,36.7,27.73a2.69,2.69,0,0,0,0,.4,2.64,2.64,0,0,0,.8,1.87,2.61,2.61,0,0,0,1.85.75h0A2.63,2.63,0,0,0,42,28.39a10.63,10.63,0,0,1,9.84-9.09.76.76,0,0,0,.69-.69.74.74,0,0,0-.57-.77,37.64,37.64,0,0,0-8.26-.95ZM39.36,29.25h0a1.15,1.15,0,0,1-.88-.41,1.12,1.12,0,0,1-.28-.93,14.61,14.61,0,0,1,5.65-9.51l0,0H44c.94,0,1.91.09,2.85.17l1.25.11-.7.33a12.52,12.52,0,0,0-3.94,3,12.16,12.16,0,0,0-2.92,6.24A1.16,1.16,0,0,1,39.36,29.25Zm4.69-10.46A14.12,14.12,0,0,0,38.62,28a.72.72,0,0,0,.18.59.74.74,0,0,0,.56.26h0a.74.74,0,0,0,.74-.69,12.66,12.66,0,0,1,3-6.48,13,13,0,0,1,3.41-2.73C45.71,18.86,44.87,18.79,44.05,18.79Z"/>
                        <path fill=${pathColor} d="M27,30h-.23a2.75,2.75,0,0,1-2.75-2.75,2.56,2.56,0,0,1,0-.39,16.06,16.06,0,0,1,2.13-6.09,1,1,0,0,1,.41-.42,33.3,33.3,0,0,1,10.15-3.21,1,1,0,0,1,.4,1.86A9.88,9.88,0,0,0,34,20.55a10.56,10.56,0,0,0-4.26,7A2.76,2.76,0,0,1,27,30Zm.73-8.08A14.26,14.26,0,0,0,26,27.15a.84.84,0,0,0,.72,1H27a.87.87,0,0,0,.85-.79,12.44,12.44,0,0,1,3.37-6.84A28.54,28.54,0,0,0,27.77,21.93Z"/>
                        <path fill=${pathColor} d="M27,30.23h-.23a3,3,0,0,1-2.93-3.39A16,16,0,0,1,26,20.67a1.24,1.24,0,0,1,.49-.5,33.48,33.48,0,0,1,10.22-3.23,1.17,1.17,0,0,1,.86.23,1.12,1.12,0,0,1,.45.76,1.18,1.18,0,0,1-.82,1.29,9.54,9.54,0,0,0-3.09,1.5A10.42,10.42,0,0,0,30,27.61a3,3,0,0,1-2.92,2.62Zm9.87-12.87h-.1a33.12,33.12,0,0,0-10.08,3.19.76.76,0,0,0-.32.32,15.92,15.92,0,0,0-2.11,6,2.19,2.19,0,0,0,0,.36,2.54,2.54,0,0,0,2.54,2.54H27a2.58,2.58,0,0,0,2.51-2.24,10.85,10.85,0,0,1,4.35-7.19,10.34,10.34,0,0,1,3.22-1.56.75.75,0,0,0,.24-1.31A.8.8,0,0,0,36.91,17.36Zm-10.1,11h-.13A1,1,0,0,1,26,27.9a1.08,1.08,0,0,1-.21-.78,14.5,14.5,0,0,1,1.8-5.3l0-.05.05,0a29.71,29.71,0,0,1,3.5-1.46L32,20l-.62.64a12.11,12.11,0,0,0-3.31,6.72,1.06,1.06,0,0,1-1,1Zm1.11-6.23a13.76,13.76,0,0,0-1.71,5.09.58.58,0,0,0,.12.47.6.6,0,0,0,.41.24H27a.66.66,0,0,0,.64-.6A12.59,12.59,0,0,1,30.51,21C29.63,21.3,28.76,21.68,27.92,22.09Z"/>
                        <path fill=${pathColor} d="M50.83,31.92a2.86,2.86,0,0,1-2.9-2.82,3,3,0,0,1,0-.52,13.43,13.43,0,0,1,7.33-9.64,1,1,0,0,1,.75,0,29.11,29.11,0,0,1,5.46,2.68A1,1,0,0,1,61,23.34a7.64,7.64,0,0,0-7.41,6.28A2.85,2.85,0,0,1,50.83,31.92Zm5-11.08a11.47,11.47,0,0,0-5.93,8.07v.16a1,1,0,0,0,1.9.22,9.55,9.55,0,0,1,6.46-7.38C57.45,21.52,56.64,21.17,55.81,20.84Z"/>
                        <path fill=${pathColor} d="M50.79,32.13a3.07,3.07,0,0,1-3.07-3,3.39,3.39,0,0,1,0-.55,13.7,13.7,0,0,1,7.45-9.8,1.2,1.2,0,0,1,.93,0,29.94,29.94,0,0,1,5.48,2.69,1.17,1.17,0,0,1-.57,2.11,7.45,7.45,0,0,0-7.22,6.11,3.07,3.07,0,0,1-3,2.47Zm4.89-13.05a.72.72,0,0,0-.29.06,13.24,13.24,0,0,0-7.21,9.48,2.59,2.59,0,0,0,0,.48,2.65,2.65,0,0,0,.8,1.86,2.77,2.77,0,0,0,1.89.75h0a2.67,2.67,0,0,0,2.59-2.13A7.84,7.84,0,0,1,61,23.13a.76.76,0,0,0,.51-.3.75.75,0,0,0-.16-1A29.4,29.4,0,0,0,56,19.14.8.8,0,0,0,55.68,19.08ZM50.86,30.24h0a1.18,1.18,0,0,1-1.16-1.17v-.16a11.71,11.71,0,0,1,6.05-8.26l.08,0,.09,0c.88.35,1.68.7,2.45,1.08l.47.23-.5.16A9.3,9.3,0,0,0,52,29.33,1.17,1.17,0,0,1,50.86,30.24Zm5-9.17A11.21,11.21,0,0,0,50.09,29v.12a.73.73,0,0,0,.76.74.75.75,0,0,0,.73-.57,9.7,9.7,0,0,1,6.11-7.36C57.09,21.6,56.48,21.33,55.82,21.07Z"/>
                        <path fill=${pathColor} d="M58.65,39.55H27.4a12.3,12.3,0,0,1-6.83-2.07,1,1,0,1,1,.9-1.68l.15.1a10.41,10.41,0,0,0,5.78,1.74H58.65a10.46,10.46,0,0,0,5.78-1.74,1,1,0,0,1,1.29.39,1,1,0,0,1-.25,1.19A12.24,12.24,0,0,1,58.65,39.55Z"/>
                        <path fill=${pathColor} d="M58.67,39.76H27.37a12.46,12.46,0,0,1-6.92-2.1,1.17,1.17,0,0,1,1.12-2l.18.12a10.1,10.1,0,0,0,5.63,1.7H58.66a10.14,10.14,0,0,0,5.65-1.71,1.16,1.16,0,0,1,1.3,1.92A12.41,12.41,0,0,1,58.67,39.76ZM27.4,39.34H58.67a12,12,0,0,0,6.69-2,.73.73,0,0,0,.17-.91.69.69,0,0,0-.44-.36.73.73,0,0,0-.56,0,10.52,10.52,0,0,1-5.87,1.77H27.38a10.53,10.53,0,0,1-5.88-1.78L21.37,36a.74.74,0,0,0-1,.31.73.73,0,0,0,.3,1,12,12,0,0,0,6.7,2.05Z"/>
                        <path fill=${pathColor} d="M32.53,15a1,1,0,0,1-.67-.28,3.82,3.82,0,0,1,0-5.39h0a1.91,1.91,0,0,0,0-2.7,1,1,0,1,1,1.35-1.35h0a3.82,3.82,0,0,1,0,5.4h0A1.89,1.89,0,0,0,32.65,12a1.92,1.92,0,0,0,.56,1.36A1,1,0,0,1,32.53,15Z"/>
                        <path fill=${pathColor} d="M32.53,15.2a1.15,1.15,0,0,1-.82-.34,4,4,0,0,1,0-5.69,1.68,1.68,0,0,0,0-2.4,1.17,1.17,0,1,1,1.65-1.65,4,4,0,0,1,0,5.67l0,0a1.69,1.69,0,0,0,0,2.4,1.16,1.16,0,0,1,0,1.64,1.15,1.15,0,0,1-.82.34Zm0-10A.75.75,0,0,0,32,6.47,2.14,2.14,0,0,1,32.63,8,2.1,2.1,0,0,1,32,9.47a3.59,3.59,0,0,0,0,5.09.88.88,0,0,0,.52.22h0a.74.74,0,0,0,.52-.22.73.73,0,0,0,0-1,2.12,2.12,0,0,1,0-3l0,0a3.6,3.6,0,0,0,0-5.09A.79.79,0,0,0,32.53,5.2Z"/>
                        <path fill=${pathColor} d="M43,11.17a.9.9,0,0,1-.67-.27,3.81,3.81,0,0,1,0-5.4h0a1.91,1.91,0,0,0,0-2.7,1,1,0,0,1,1.34-1.35h0a3.81,3.81,0,0,1,0,5.39h0a1.9,1.9,0,0,0,0,2.69,1,1,0,0,1,0,1.35A.9.9,0,0,1,43,11.17Z"/>
                        <path fill=${pathColor} d="M43,11.39h0a1.17,1.17,0,0,1-.82-.34,4,4,0,0,1,0-5.69,1.77,1.77,0,0,0,.51-1.2A1.76,1.76,0,0,0,42.2,3a1.16,1.16,0,0,1,0-1.64,1.17,1.17,0,0,1,1.65,0,4,4,0,0,1,0,5.7,1.71,1.71,0,0,0-.5,1.2,1.68,1.68,0,0,0,.5,1.2,1.18,1.18,0,0,1,0,1.65A1.21,1.21,0,0,1,43,11.39ZM43,11h0a.72.72,0,0,0,.53-.21.76.76,0,0,0,0-1.05,2.09,2.09,0,0,1,0-3,3.61,3.61,0,0,0,0-5.1.74.74,0,0,0-1,0,.73.73,0,0,0,0,1,2.19,2.19,0,0,1,.62,1.51,2.09,2.09,0,0,1-.63,1.49A3.6,3.6,0,0,0,41.44,8.2a3.55,3.55,0,0,0,1.06,2.55A.79.79,0,0,0,43,11Z"/>
                        <path fill=${pathColor} d="M53.51,14a.92.92,0,0,1-.68-.27,3.81,3.81,0,0,1,0-5.39h0a1.91,1.91,0,0,0,0-2.7,1,1,0,0,1,0-1.34,1,1,0,0,1,1.35,0h0a3.81,3.81,0,0,1,0,5.39h0a1.93,1.93,0,0,0-.57,1.34,1.89,1.89,0,0,0,.57,1.35,1,1,0,0,1,0,1.35A.92.92,0,0,1,53.51,14Z"/>
                        <path fill=${pathColor} d="M53.51,14.25h0a1.21,1.21,0,0,1-.82-.34,4,4,0,0,1,0-5.69,1.71,1.71,0,0,0,.5-1.2,1.68,1.68,0,0,0-.5-1.2,1.18,1.18,0,0,1,0-1.65,1.15,1.15,0,0,1,.82-.34,1.23,1.23,0,0,1,.83.33,4,4,0,0,1,0,5.7,1.7,1.7,0,0,0-.5,1.2,1.64,1.64,0,0,0,.5,1.2,1.18,1.18,0,0,1,0,1.65A1.22,1.22,0,0,1,53.51,14.25Zm0-.43h0a.78.78,0,0,0,.52-.21.76.76,0,0,0,0-1,2.11,2.11,0,0,1-.63-1.5A2.07,2.07,0,0,1,54,9.57a3.62,3.62,0,0,0,0-5.1.77.77,0,0,0-.53-.22h0a.75.75,0,0,0-.53.22.76.76,0,0,0,0,1A2.11,2.11,0,0,1,53.61,7,2.15,2.15,0,0,1,53,8.51a3.62,3.62,0,0,0,0,5.1.78.78,0,0,0,.52.21Z"/>
                        </g>
                        </g>
                    </svg>
                </div>
            `);
        case 'Protein Metabolizman':
            return (`
                <div class='gs-icon-container'>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 174.28 156.11" width='${hxSize}' height='${hxSize}'>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="_1" data-name="1">
                        <path fill=${hexColor} d="M121,0H53.25A19.34,19.34,0,0,0,36.49,9.68L2.59,68.38a19.38,19.38,0,0,0,0,19.35l33.9,58.71a19.33,19.33,0,0,0,16.76,9.67H121a19.34,19.34,0,0,0,16.76-9.67l33.89-58.71a19.33,19.33,0,0,0,0-19.35L137.79,9.68A19.35,19.35,0,0,0,121,0Z"/>
                        </g>
                        </g>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 84.19 65.62" width='${gsSize}' height='${gsSize}' style='position: absolute; left: calc(50% - ${gsSize/2}px); top: calc(50% - ${gsSize/2}px)'>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="_1" data-name="1">
                        <path fill=${pathColor} d="M39.41,65.62a6,6,0,0,0,5.26-3.44,6,6,0,0,0-2.87-8l-.12-.05,1.41-6.34,4.37-3.63a28.56,28.56,0,0,0,8.73-12.58L60,20.72c.13-.32.24-.64.34-1l.15-.42h0c.06-.23.14-.47.19-.71a15,15,0,0,0-29.2-7h0l0,.24c0,.09-.06.18-.08.28A15.31,15.31,0,0,0,31,14l-1.56,9.55a28.61,28.61,0,0,0,3.24,18.49L34.86,46l-1.39,6.3A6,6,0,1,0,35.18,64,6,6,0,0,0,39.41,65.62Zm-6-51.28.33-2a12.62,12.62,0,0,1,24.56,5.77c-.07.3-.16.6-.25.9l-.34,1a12.6,12.6,0,0,1-24.3-5.63Zm8.29,31.47-4.75-1.05L34.77,40.9A26.16,26.16,0,0,1,31.8,24l.42-2.56A15,15,0,0,0,52,29.13a14.81,14.81,0,0,0,3.2-1.91l-1.24,3.56a26.12,26.12,0,0,1-8,11.52l-4.25,3.53ZM35,61.53a3.58,3.58,0,0,1-3.22,0,3.61,3.61,0,0,1,2.41-6.75,1.19,1.19,0,0,0,1.43-.91l1.47-6.63,3.52.78L39.1,54.66A1.2,1.2,0,0,0,40,56.09a3.61,3.61,0,0,1-.67,7.13,3.54,3.54,0,0,1-2.91-1.37A1.12,1.12,0,0,0,35,61.53Z"/>
                        <path fill=${pathColor} d="M51.23,16.53a5.41,5.41,0,1,0-6.45,4.11A5.42,5.42,0,0,0,51.23,16.53ZM43,14.71A3,3,0,1,1,45.3,18.3,3,3,0,0,1,43,14.71Z"/>
                        <path fill=${pathColor} d="M69.89,65.55a2.29,2.29,0,0,1-.78-.14L58,61.41a2.29,2.29,0,0,1-1.38-2.93l4-11.21a2.3,2.3,0,0,1,2.94-1.38,2.27,2.27,0,0,1,1.3,1.19l1.52,3.24A25.12,25.12,0,0,0,64.17,9.66,2.28,2.28,0,0,1,63.25,7h0l1.36-3.67A2.31,2.31,0,0,1,68,2.1a33.56,33.56,0,0,1,2.1,56.09L72,62.28a2.31,2.31,0,0,1-2.08,3.27ZM59.39,59,69,62.41l-2.43-5.2,1-.66A30.77,30.77,0,0,0,67,4.79L66,7.54a27.9,27.9,0,0,1,.65,46.06l-1.35,1-2.45-5.25Z"/>
                        <path fill=${pathColor} d="M17.57,63.86a2.29,2.29,0,0,1-1.17-.32A33.55,33.55,0,0,1,14.13,7.35l-1.9-4.08A2.3,2.3,0,0,1,13.34.21,2.36,2.36,0,0,1,15.09.13l11.1,4a2.3,2.3,0,0,1,1.39,2.93h0l-4,11.19a2.3,2.3,0,0,1-4.25.19L17.8,15.22a25.07,25.07,0,0,0,2.31,40.67,2.26,2.26,0,0,1,.94,2.68l-1.3,3.75a2.31,2.31,0,0,1-2.18,1.55ZM15.22,3.13l2.43,5.2-1,.66a30.77,30.77,0,0,0,.67,51.86l1-2.83a27.84,27.84,0,0,1-.77-46.08L18.89,11l2.44,5.24L24.8,6.58Z"/>
                        </g>
                        </g>
                    </svg>
                </div>
            `);
        case 'Yağ Metabolizman':
            return (`
                <div class='gs-icon-container'>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 174.28 156.11" width='${hxSize}' height='${hxSize}'>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="_1" data-name="1">
                        <path fill=${hexColor} d="M121,0H53.25A19.34,19.34,0,0,0,36.49,9.68L2.59,68.38a19.38,19.38,0,0,0,0,19.35l33.9,58.71a19.33,19.33,0,0,0,16.76,9.67H121a19.34,19.34,0,0,0,16.76-9.67l33.89-58.71a19.33,19.33,0,0,0,0-19.35L137.79,9.68A19.35,19.35,0,0,0,121,0Z"/>
                        </g>
                        </g>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 84.29 65.62" width='${gsSize}' height='${gsSize}' style='position: absolute; left: calc(50% - ${gsSize/2}px); top: calc(50% - ${gsSize/2}px)'>
                        <g id="Layer_2" data-name="Layer 2"><g id="_1" data-name="1">
                        <path fill=${pathColor} d="M41.39,11.27a1.18,1.18,0,0,0-.25.25C40.6,12.38,27.8,32.75,27.8,40.3A14.38,14.38,0,0,0,53,49.64a1.15,1.15,0,0,0-1.75-1.49A12.13,12.13,0,0,1,30,40.3C30,35,38,21,42.11,14.31,46.21,21,54.24,35,54.24,40.3a11.91,11.91,0,0,1-1,4.67,1.22,1.22,0,1,0,2.12.9,14.14,14.14,0,0,0,1.13-5.59c0-7.53-12.85-27.9-13.4-28.76A1.2,1.2,0,0,0,41.39,11.27Z"/>
                        <path fill=${pathColor} d="M42.11,47.2a6.9,6.9,0,0,1-6.9-6.9,1.15,1.15,0,0,0-2.3,0,9.21,9.21,0,0,0,9.2,9.19,1.15,1.15,0,0,0,1.27-1,1.16,1.16,0,0,0-1-1.27A1,1,0,0,0,42.11,47.2Z"/>
                        <path fill=${pathColor} d="M68,2.1a2.32,2.32,0,0,0-3.35,1.19L63.33,7a2.27,2.27,0,0,0,.91,2.72,25.15,25.15,0,0,1,2.22,40.71L65,47.13a2.32,2.32,0,0,0-1.31-1.19,2.29,2.29,0,0,0-2.93,1.38l-4,11.23a2.29,2.29,0,0,0,1.38,2.93l11.13,4a2.28,2.28,0,0,0,.78.13,2.35,2.35,0,0,0,1-.21,2.3,2.3,0,0,0,1.11-3.06l-1.91-4.09A33.6,33.6,0,0,0,68,2.1Zm-.42,54.52-1,.66,2.44,5.21L59.46,59l3.47-9.67,2.44,5.25,1.36-1a27.93,27.93,0,0,0-.65-46.11l1-2.76a30.82,30.82,0,0,1,.52,51.83Z"/>
                        <path fill=${pathColor} d="M26.22,4.13,15.11.14a2.26,2.26,0,0,0-1.75.08,2.29,2.29,0,0,0-1.12,3.06l1.91,4.08a33.59,33.59,0,0,0,2.27,56.26,2.29,2.29,0,0,0,1.17.32h0a2.34,2.34,0,0,0,2.19-1.56l1.3-3.75A2.29,2.29,0,0,0,20.14,56a25.08,25.08,0,0,1-2.32-40.71l1.51,3.24a2.31,2.31,0,0,0,4.26-.2l4-11.21h0A2.29,2.29,0,0,0,26.22,4.13ZM21.36,16.25,18.91,11l-1.36,1A27.93,27.93,0,0,0,6.24,40.19a27.62,27.62,0,0,0,12.09,17.9l-1,2.84A30.82,30.82,0,0,1,16.67,9l1-.66-2.43-5.2,9.59,3.45Z"/>
                        </g>
                        </g>
                    </svg>
                </div>
            `);
        case 'Bağırsak Hareketliliğin':
            return (`
                <div class='gs-icon-container'>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 174.28 156.11" width='${hxSize}' height='${hxSize}'>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="_1" data-name="1">
                        <path fill=${hexColor} d="M121,0H53.25A19.34,19.34,0,0,0,36.49,9.68L2.59,68.38a19.38,19.38,0,0,0,0,19.35l33.9,58.71a19.33,19.33,0,0,0,16.76,9.67H121a19.34,19.34,0,0,0,16.76-9.67l33.89-58.71a19.33,19.33,0,0,0,0-19.35L137.79,9.68A19.35,19.35,0,0,0,121,0Z"/>
                        </g>
                        </g>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 81.53 81.53" width='${gsSize}' height='${gsSize}' style='position: absolute; left: calc(50% - ${gsSize/2}px); top: calc(50% - ${gsSize/2}px)'>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="_1" data-name="1">
                        <path fill=${pathColor} d="M79.87,25A9.2,9.2,0,0,0,67.06,12.18a9.16,9.16,0,0,0-10.52,0,9.16,9.16,0,0,0-10.52,0,9.15,9.15,0,0,0-10.51,0,9.18,9.18,0,0,0-10.53,0V4.52L17.46,0h-3V12.18A9.2,9.2,0,1,0,25,27.27a9.16,9.16,0,0,0,10.52,0,9.16,9.16,0,0,0,10.52,0,9.16,9.16,0,0,0,10.52,0,9.23,9.23,0,0,0,5.26,1.66,9.76,9.76,0,0,0,1.44-.12,9.74,9.74,0,0,0-.12,1.43,8.78,8.78,0,0,0,.12,1.45,9.42,9.42,0,0,0-6.69,1.54,9.16,9.16,0,0,0-10.53,0,9.16,9.16,0,0,0-10.52,0,9.16,9.16,0,0,0-10.52,0,9.16,9.16,0,0,0-10.52,0A9.21,9.21,0,0,0,1.65,46a9.17,9.17,0,0,0,0,10.51,9.19,9.19,0,0,0,12.8,12.8,9.16,9.16,0,0,0,10.52,0A9.15,9.15,0,0,0,30.24,71a9.26,9.26,0,0,0,5.27-1.66,9.16,9.16,0,0,0,10.52,0,9.15,9.15,0,0,0,10.51,0V77l7.53,4.52h3V69.35A9.2,9.2,0,0,0,61.8,52.6a9.15,9.15,0,0,0-5.26,1.66,9.16,9.16,0,0,0-10.52,0,9.16,9.16,0,0,0-10.52,0,9.16,9.16,0,0,0-10.52,0,9.15,9.15,0,0,0-5.26-1.66,9.87,9.87,0,0,0-1.44.12,8.57,8.57,0,0,0,.13-1.44,8.66,8.66,0,0,0-.13-1.44,8.76,8.76,0,0,0,1.44.13A9.16,9.16,0,0,0,25,48.31a9.16,9.16,0,0,0,10.52,0A9.25,9.25,0,0,0,40.76,50,9.15,9.15,0,0,0,46,48.31a9.16,9.16,0,0,0,10.52,0,9.16,9.16,0,0,0,10.52,0A9.2,9.2,0,0,0,79.87,35.5a9.15,9.15,0,0,0,0-10.51ZM77.21,36.38A6.56,6.56,0,0,1,69,46.44a9.09,9.09,0,0,0,2-5.68H68.38A6.58,6.58,0,0,1,55.6,43l-2.48.87a9.47,9.47,0,0,0,1.44,2.61,6.74,6.74,0,0,1-7.66-.79L46,44.87l-.87.78A6.56,6.56,0,0,1,34.59,43l-2.47.9a8.94,8.94,0,0,0,1.4,2.51,6.85,6.85,0,0,1-6.56,0,9.13,9.13,0,0,0,2-5.67H26.3a6.59,6.59,0,0,1-6.58,6.58,6.33,6.33,0,0,1-2.44-.49,9.27,9.27,0,0,0-3.19-3.37l-1.4,2.23a6.53,6.53,0,0,1,3.09,5.57,6.61,6.61,0,0,1-.48,2.46,9.2,9.2,0,0,0-4.78,8.06h2.63a6.57,6.57,0,0,1,11-4.89l.87.79.88-.79a6.77,6.77,0,0,1,7.67-.78,9.21,9.21,0,0,0-2,5.67h2.63a6.57,6.57,0,0,1,2.19-4.89,6.5,6.5,0,0,1,4.38-1.68,6.58,6.58,0,0,1,6.58,6.57H50a9.15,9.15,0,0,0-2-5.67,6.77,6.77,0,0,1,7.67.78l.87.79.88-.79a6.57,6.57,0,1,1,7.67,10.58l-.66.38V78.68l-5.26-3.16V67.87l-.65-.38a6.51,6.51,0,0,1-1.1-.79l-.88-.79-.87.78a6.56,6.56,0,0,1-10.54-2.56l-2.46.94A9.23,9.23,0,0,0,44,67.48a6.85,6.85,0,0,1-6.56,0,9.07,9.07,0,0,0,1.46-2.64L36.46,64a6.57,6.57,0,0,1-10.6,2.72L25,65.91l-.87.78a6.75,6.75,0,0,1-7.67.79,9.12,9.12,0,0,0,1.46-2.65L15.41,64A6.57,6.57,0,1,1,9.2,55.23V52.6a9.08,9.08,0,0,0-5.67,2,6.48,6.48,0,0,1,0-6.57,9.15,9.15,0,0,0,4.2,1.84l.42-2.6A6.57,6.57,0,0,1,9.2,34.19a6.61,6.61,0,0,1,4.39,1.68l.87.79.88-.79A6.77,6.77,0,0,1,23,35.09a9.19,9.19,0,0,0-1.45,2.6l2.48.88a6.58,6.58,0,0,1,12.78,2.19h2.63a9.1,9.1,0,0,0-2-5.66,6.73,6.73,0,0,1,7.67.78l.87.78.88-.79a6.57,6.57,0,0,1,11,4.89h2.63a9.15,9.15,0,0,0-2-5.67,6.7,6.7,0,0,1,5.76-.4,9.43,9.43,0,0,0,1.2,1.69l2-1.76a6.52,6.52,0,0,1-1.19-6.85,9,9,0,0,0,1.7-1.2l-1.75-1.95a6.58,6.58,0,0,1-10.59-2.7l-2.48.88a9.56,9.56,0,0,0,1.44,2.6,6.75,6.75,0,0,1-7.66-.78L46,23.83l-.87.79a6.77,6.77,0,0,1-7.67.78,9,9,0,0,0,1.44-2.6l-2.48-.88a6.57,6.57,0,0,1-10.58,2.7L25,23.83l-.87.79A6.57,6.57,0,1,1,16.44,14l.65-.38V2.85L22.35,6v7.66L23,14a6.51,6.51,0,0,1,1.1.79l.87.79.88-.79a6.77,6.77,0,0,1,7.67-.78,9.18,9.18,0,0,0-2,5.67h2.63a6.57,6.57,0,0,1,11-4.89l.87.79.88-.79a6.57,6.57,0,0,1,11,4.89h2.63a9.13,9.13,0,0,0-2-5.66,6.73,6.73,0,0,1,7.67.78l.87.78.88-.79A6.56,6.56,0,0,1,78,23a9.09,9.09,0,0,0-5.68-2v2.63a6.56,6.56,0,0,1,4.89,11l-.78.87Z"/>
                        </g>
                        </g>
                    </svg>
                </div>
            `);
        case 'Laktoz Hassasiyetin':
            return (`
                <div class='gs-icon-container'>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 174.28 156.11" width='${hxSize}' height='${hxSize}'>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="_1" data-name="1">
                        <path fill=${hexColor} d="M121,0H53.25A19.34,19.34,0,0,0,36.49,9.68L2.59,68.38a19.38,19.38,0,0,0,0,19.35l33.9,58.71a19.33,19.33,0,0,0,16.76,9.67H121a19.34,19.34,0,0,0,16.76-9.67l33.89-58.71a19.33,19.33,0,0,0,0-19.35L137.79,9.68A19.35,19.35,0,0,0,121,0Z"/>
                        </g>
                        </g>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 58.81 96.12" width='${gsSize}' height='${gsSize}' style='position: absolute; left: calc(50% - ${gsSize/2}px); top: calc(50% - ${gsSize/2}px)'>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="_1" data-name="1">
                        <path fill=${pathColor} d="M36.05,52.71V47.63A64.49,64.49,0,0,0,35,35.82a1.19,1.19,0,0,0-1.18-1H7.09a1.19,1.19,0,0,0-1.18,1,64.41,64.41,0,0,0-1.1,11.81V90.11A1.2,1.2,0,0,0,6,91.31H34.85a1.2,1.2,0,0,0,1.2-1.2V64.64l-2.4-2.89V88.91H7.21V47.63a62.82,62.82,0,0,1,.88-10.39H32.76a61.77,61.77,0,0,1,.89,10.39v5.08Zm2.4,14.81V90.11a3.6,3.6,0,0,1-3.6,3.6H6a3.6,3.6,0,0,1-3.6-3.6V47.63A66.67,66.67,0,0,1,12.68,12h15.5a66.36,66.36,0,0,1,7.93,18.06V22.44a68.49,68.49,0,0,0-5.44-11A3.57,3.57,0,0,0,32.44,8.4V3.6A3.6,3.6,0,0,0,28.84,0H12A3.61,3.61,0,0,0,8.41,3.6V8.4a3.59,3.59,0,0,0,1.77,3.09A69.12,69.12,0,0,0,0,47.63V90.11a6,6,0,0,0,6,6H34.85a6,6,0,0,0,6-6V70.4ZM22.23,7.21H10.81V3.6A1.21,1.21,0,0,1,12,2.4H28.84A1.2,1.2,0,0,1,30,3.6V7.21h-4.2a.6.6,0,1,0,0,1.19H30a1.2,1.2,0,0,1-1.2,1.21H12A1.21,1.21,0,0,1,10.81,8.4H22.23a.6.6,0,1,0,0-1.19Zm13.82,45.5V47.63A64.49,64.49,0,0,0,35,35.82a1.19,1.19,0,0,0-1.18-1H7.09a1.19,1.19,0,0,0-1.18,1,64.41,64.41,0,0,0-1.1,11.81V90.11A1.2,1.2,0,0,0,6,91.31H34.85a1.2,1.2,0,0,0,1.2-1.2V64.64l-2.4-2.89V88.91H7.21V47.63a62.82,62.82,0,0,1,.88-10.39H32.76a61.77,61.77,0,0,1,.89,10.39v5.08Zm0,0V47.63A64.49,64.49,0,0,0,35,35.82a1.19,1.19,0,0,0-1.18-1H7.09a1.19,1.19,0,0,0-1.18,1,64.41,64.41,0,0,0-1.1,11.81V90.11A1.2,1.2,0,0,0,6,91.31H34.85a1.2,1.2,0,0,0,1.2-1.2V64.64l-2.4-2.89V88.91H7.21V47.63a62.82,62.82,0,0,1,.88-10.39H32.76a61.77,61.77,0,0,1,.89,10.39v5.08Z"/>
                        <path fill=${pathColor} d="M58.53,57l-12,14.36a1.19,1.19,0,0,1-1.84,0l-3.87-4.64L36.05,61l-2.4-2.88L32.75,57a1.2,1.2,0,0,1,.9-2h4.81V19.15a1.2,1.2,0,1,1,2.39,0v37.1a1.2,1.2,0,0,1-1.2,1.2H36.23l2.22,2.67L40.85,63l4.79,5.76,9.41-11.3H51.63a1.2,1.2,0,0,1-1.2-1.2V18a1.2,1.2,0,1,1,2.39,0v37.1h4.79a1.19,1.19,0,0,1,1.08.69A1.22,1.22,0,0,1,58.53,57Z"/>
                        <path fill=${pathColor} d="M52.82,13.16a1.2,1.2,0,1,1-2.39,0,1.2,1.2,0,1,1,2.39,0Z"/>
                        <path fill=${pathColor} d="M52.82,1.19V8.37a1.2,1.2,0,1,1-2.39,0V1.19a1.2,1.2,0,0,1,2.39,0Z"/>
                        <path fill=${pathColor} d="M48,23.93a1.2,1.2,0,1,1-1.19-1.19A1.19,1.19,0,0,1,48,23.93Z"/>
                        <path fill=${pathColor} d="M48,19.15A1.2,1.2,0,1,1,46.84,18,1.19,1.19,0,0,1,48,19.15Z"/>
                        <path fill=${pathColor} d="M48,14.36a1.2,1.2,0,0,1-2.39,0,1.2,1.2,0,1,1,2.39,0Z"/>
                        <path fill=${pathColor} d="M48,28.72V39.49H45.64V28.72a1.2,1.2,0,0,1,2.39,0Z"/>
                        <path fill=${pathColor} d="M40.85,9.57v4.79a1.2,1.2,0,0,1-2.39,0V9.57a1.2,1.2,0,1,1,2.39,0Z"/>
                        <path fill=${pathColor} d="M40.85,4.78a1.2,1.2,0,1,1-1.2-1.2A1.19,1.19,0,0,1,40.85,4.78Z"/>
                        </g>
                        </g>
                    </svg>
                </div>
            `);
        case 'Vitamin Sentezin':
            return (`
                <div class='gs-icon-container'>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 174.28 156.11" width='${hxSize}' height='${hxSize}'>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="_1" data-name="1">
                        <path fill=${hexColor} d="M121,0H53.25A19.34,19.34,0,0,0,36.49,9.68L2.59,68.38a19.38,19.38,0,0,0,0,19.35l33.9,58.71a19.33,19.33,0,0,0,16.76,9.67H121a19.34,19.34,0,0,0,16.76-9.67l33.89-58.71a19.33,19.33,0,0,0,0-19.35L137.79,9.68A19.35,19.35,0,0,0,121,0Z"/>
                        </g>
                        </g>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 84 87.09" width='${gsSize}' height='${gsSize}' style='position: absolute; left: calc(50% - ${gsSize/2}px); top: calc(50% - ${gsSize/2}px)'>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="_1" data-name="1">
                        <path fill=${pathColor} d="M26.73,40.07a1.35,1.35,0,0,0-1.7.19l-4,4L4.53,60.76.41,64.88a1.36,1.36,0,0,0-.19,1.71A19.23,19.23,0,1,0,26.73,40.07Zm-4.5,6.87A11,11,0,1,1,7.09,62.08Zm-5.8,25.83A16.28,16.28,0,0,1,3.16,66l2-2A13.74,13.74,0,1,0,24.34,45l1.95-2a16.29,16.29,0,0,1,6.63,13.27A16.49,16.49,0,0,1,16.43,72.77Z"/>
                        <path fill=${pathColor} d="M19.59,61.36a1.3,1.3,0,0,0,1.86.06l.06-.06a1,1,0,0,0,.42-.44,1.14,1.14,0,0,0,0-.52,1.31,1.31,0,0,0-.42-1A1.34,1.34,0,0,0,20,59a1,1,0,0,0-.44.3,1.27,1.27,0,0,0-.41,1.07,1.14,1.14,0,0,0,0,.52A1,1,0,0,0,19.59,61.36Z"/>
                        <path fill=${pathColor} d="M19.59,55.87a1.31,1.31,0,0,0,1.86.06l.06-.06a1,1,0,0,0,.42-.44,1.14,1.14,0,0,0,0-.52,1.28,1.28,0,0,0-.42-1A1.35,1.35,0,0,0,20,53.53a1,1,0,0,0-.44.31,1.27,1.27,0,0,0-.41,1.07,1.14,1.14,0,0,0,0,.52A1,1,0,0,0,19.59,55.87Z"/>
                        <path fill=${pathColor} d="M14.09,61.36a1.32,1.32,0,0,0,1.87.06l.06-.06a1,1,0,0,0,.41-.44,1.14,1.14,0,0,0,0-.52,1.34,1.34,0,0,0-.41-1A1.35,1.35,0,0,0,14.53,59a1,1,0,0,0-.44.3,1.3,1.3,0,0,0-.41,1.07,1.14,1.14,0,0,0,0,.52A1,1,0,0,0,14.09,61.36Z"/>
                        <path fill=${pathColor} d="M39.46,86.41a2.24,2.24,0,0,1-.45-.64l-5-10.7A2.3,2.3,0,0,1,35.07,72l10.78-5.08A2.29,2.29,0,0,1,49,69.79l-1.23,3.37A25.18,25.18,0,0,0,75,42.8a2.25,2.25,0,0,1,1.27-2.56h0l3.56-1.63a2.3,2.3,0,0,1,2,0A2.25,2.25,0,0,1,83,40.14,33.58,33.58,0,0,1,44.79,81.32l-1.54,4.25a2.31,2.31,0,0,1-3,1.38A2.42,2.42,0,0,1,39.46,86.41ZM36.69,74.33,41,83.55l2-5.41,1.18.24a30.81,30.81,0,0,0,36.28-37L77.77,42.6A27.94,27.94,0,0,1,45.63,75.67L44,75.39l2-5.45Z"/>
                        <path fill=${pathColor} d="M3.62,48.19A2.34,2.34,0,0,1,3,47.13,33.59,33.59,0,0,1,41.19,5.75l1.54-4.23A2.31,2.31,0,0,1,47,1.32L52,12a2.29,2.29,0,0,1-1.09,3.05h0L40.14,20.15A2.29,2.29,0,0,1,37.08,19,2.27,2.27,0,0,1,37,17.27l1.23-3.35A25.07,25.07,0,0,0,11.07,44.34a2.3,2.3,0,0,1-1.24,2.57L6.26,48.64a2.32,2.32,0,0,1-2.65-.45ZM45,3.54l-2,5.4L41.81,8.7A30.81,30.81,0,0,0,5.57,45.88l2.71-1.31A27.86,27.86,0,0,1,40.35,11.41l1.64.28-2,5.44,9.29-4.37Z"/>
                        <path fill=${pathColor} d="M71.62,38h-9a2.87,2.87,0,0,1-2.86-2.87v-4h-4a2.88,2.88,0,0,1-2.87-2.87v-9a2.88,2.88,0,0,1,2.87-2.87h4v-4A2.87,2.87,0,0,1,62.6,9.51h9a2.88,2.88,0,0,1,2.87,2.87v4h4a2.87,2.87,0,0,1,2.86,2.87v9a2.87,2.87,0,0,1-2.86,2.87h-4v4A2.88,2.88,0,0,1,71.62,38ZM55.73,18.94a.32.32,0,0,0-.32.32v9a.32.32,0,0,0,.32.32h6.56v6.56a.31.31,0,0,0,.31.32h9a.32.32,0,0,0,.32-.32V28.59H78.5a.32.32,0,0,0,.32-.32v-9a.32.32,0,0,0-.32-.32H71.94V12.38a.32.32,0,0,0-.32-.32h-9a.32.32,0,0,0-.31.32v6.56Z"/>
                        </g>
                        </g>
                    </svg>
                </div>
            `);
        case 'Şeker Tüketimin':
            return (`
                <div class='gs-icon-container'>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 174.28 156.11" width='${hxSize}' height='${hxSize}'>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="_1" data-name="1">
                        <path fill=${hexColor} d="M121,0H53.25A19.34,19.34,0,0,0,36.49,9.68L2.59,68.38a19.38,19.38,0,0,0,0,19.35l33.9,58.71a19.33,19.33,0,0,0,16.76,9.67H121a19.34,19.34,0,0,0,16.76-9.67l33.89-58.71a19.33,19.33,0,0,0,0-19.35L137.79,9.68A19.35,19.35,0,0,0,121,0Z"/>
                        </g>
                        </g>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 57.33 73.7" width='${gsSize}' height='${gsSize}' style='position: absolute; left: calc(50% - ${gsSize/2}px); top: calc(50% - ${gsSize/2}px)'>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="_1" data-name="1">
                        <path fill=${pathColor} d="M33,50.69v3.44a4,4,0,0,1-2.39,3.64,10,10,0,0,0,.45,2.64,6.59,6.59,0,0,0,4.58-6.28V48.44A10.55,10.55,0,0,0,33,50.69Zm2.64-45.4H34.33v-4A1.33,1.33,0,0,0,33,0H4A1.33,1.33,0,0,0,2.64,1.33v4H1.32A1.33,1.33,0,0,0,0,6.61v5.28a1.33,1.33,0,0,0,1.32,1.32H4v3.56L1.55,20.4a1.26,1.26,0,0,0-.23.73v33a6.61,6.61,0,0,0,6.61,6.6h14a19.2,19.2,0,0,1-.28-2.64H7.93a4,4,0,0,1-4-4V43.22c2.15-.67,7.61-2.29,10.57-2.29a12.3,12.3,0,0,1,5.61,1.34c.9.37,1.92.8,3.21,1.23.56.19,1.12.42,1.73.67.41.19.82.36,1.24.52a18.22,18.22,0,0,1,2.06-2.06,19.42,19.42,0,0,1-2.29-.9c-.67-.27-1.3-.52-1.91-.73-1.18-.4-2.16-.81-3-1.17a15.15,15.15,0,0,0-6.64-1.54A46,46,0,0,0,4,40.45V21.52l2-3H31l2,3V39.68a17.33,17.33,0,0,1,2.64-1V21.13a1.33,1.33,0,0,0-.22-.73L33,16.77V13.21h2.64A1.32,1.32,0,0,0,37,11.89V6.61A1.32,1.32,0,0,0,35.65,5.29ZM30.37,15.85H6.61V13.21H30.37Zm4-5.28H2.64V7.93H14.53V5.29H5.28V2.65H31.69V5.29H26.41V7.93h7.92Z"/>
                        <rect fill=${pathColor} x="17.17" y="5.29" width="2.64" height="2.64"/>
                        <rect fill=${pathColor} x="7.93" y="44.89" width="2.64" height="2.64"/>
                        <rect fill=${pathColor} x="13.21" y="47.53" width="2.64" height="2.64"/>
                        <path fill=${pathColor} d="M22.14,52.81a19.15,19.15,0,0,0-.45,2.64h-.56V52.81Z"/>
                        <path fill=${pathColor} d="M41,44A13.37,13.37,0,1,1,27.6,57.33,13.36,13.36,0,0,1,41,44m0,17.62a1.83,1.83,0,0,0,1.83-1.83V48.83a1.83,1.83,0,1,0-3.65,0V59.76A1.83,1.83,0,0,0,41,61.59m0,6.07h0a1.82,1.82,0,1,0-1.83-1.82A1.82,1.82,0,0,0,41,67.66M41,41h0A16.37,16.37,0,1,0,57.33,57.33,16.38,16.38,0,0,0,41,41Z"/>
                        </g>
                        </g>
                    </svg>
                </div>
            `);
        case 'Probiyotik Organizmaların':
            return (`
                <div class='gs-icon-container'>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 174.28 156.11" width='${hxSize}' height='${hxSize}'>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="_1" data-name="1">
                        <path fill=${hexColor} d="M121,0H53.25A19.34,19.34,0,0,0,36.49,9.68L2.59,68.38a19.38,19.38,0,0,0,0,19.35l33.9,58.71a19.33,19.33,0,0,0,16.76,9.67H121a19.34,19.34,0,0,0,16.76-9.67l33.89-58.71a19.33,19.33,0,0,0,0-19.35L137.79,9.68A19.35,19.35,0,0,0,121,0Z"/>
                        </g>
                        </g>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 97.95 98.99" width='${gsSize}' height='${gsSize}' style='position: absolute; left: calc(50% - ${gsSize/2}px); top: calc(50% - ${gsSize/2}px)'>
                        <defs>
                        <style>.main-po-cls{fill:none;stroke-linejoin:round;stroke-width:2px;}</style>
                        </defs>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="_1" data-name="1">
                        <path stroke=${pathColor} class="main-po-cls" d="M31,71.86a6.92,6.92,0,0,1-1.07.47,7.13,7.13,0,0,1-3.51.35,7,7,0,0,1-3,1.92c-2.81,1-5.65.22-6.33-1.71s.93-4.14,3.58-5.19A7.61,7.61,0,0,0,22.86,69a7.05,7.05,0,0,0,3.5.44,6.91,6.91,0,0,0,2.89,2A7.28,7.28,0,0,0,31,71.86Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M52.76,38.33a6.83,6.83,0,0,1-1.2.23,7.45,7.45,0,0,1-5.18,3.81,6.71,6.71,0,0,1-3.14,2.81,7,7,0,0,1-3.46.66A7.08,7.08,0,0,1,37,48c-.25.11-.51.21-.76.3a7.4,7.4,0,0,1-3.47,5.51,7.13,7.13,0,0,1-1,3.37c-1.52,2.56-4.19,3.79-6,2.74s-1.95-4-.43-6.54a7.07,7.07,0,0,1,2.47-2.51,7.1,7.1,0,0,1,1-3.37,7.51,7.51,0,0,1,1.45-1.77c0-1.65,1.41-3.48,3.6-4.49a7,7,0,0,1,3.46-.66,7,7,0,0,1,2.76-2.19,7.33,7.33,0,0,1,2.48-.66A7.46,7.46,0,0,1,48,33.85a7.13,7.13,0,0,0,2.05,2.43,7.6,7.6,0,0,0,2,1.1A3.28,3.28,0,0,0,52.76,38.33Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M62.83,24.52a7,7,0,0,1-2.2,2.75A7,7,0,0,1,60,30.72a7.64,7.64,0,0,1-1.25,1.9h0a6.77,6.77,0,0,0-1-.13l-.49,0c-.24,0-.47,0-.7,0a7.55,7.55,0,0,0-1.9-2.13c-.2-.15-.41-.3-.62-.43h0a7.53,7.53,0,0,0-1.31-.64,7.76,7.76,0,0,1,.56-1.71,7,7,0,0,1,2.2-2.75,7.15,7.15,0,0,1,.68-3.46c1.26-2.7,3.79-4.19,5.65-3.33S64.09,21.81,62.83,24.52Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M89.38,32.44c-1.66,1.15-4.35.15-6-2.22,1.56-1.29,2.29-3,1.68-4.46-.76-1.74-3.27-2.35-5.81-1.5-1.55-2.41-1.5-5.2.13-6.34s4.44-.12,6.13,2.33a7.06,7.06,0,0,1,1.26,3.29,7,7,0,0,1,2.63,2.34C91.06,28.33,91.07,31.27,89.38,32.44Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M96.79,50.7a7.05,7.05,0,0,1-1.52,3.18,7.12,7.12,0,0,1,.13,3.52,7.39,7.39,0,0,1-.7,2,3.07,3.07,0,0,0-.25-.26c-1.41-1.27-4-.79-5.91,1a6.68,6.68,0,0,1-.42-4.25,7.16,7.16,0,0,1,1.52-3.18,6.74,6.74,0,0,1-.29-1.89,2.8,2.8,0,0,0,1.59-.34c1.51-.87,1.92-3.11,1.1-5.35a3,3,0,0,1,2.21-.47C96.26,45.07,97.39,47.78,96.79,50.7Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M9,29.05a6.84,6.84,0,0,1,.22,3c-.7-1.74-2-2.9-3.48-2.83-2,.11-3.59,2.6-3.44,5.58,0,.25,0,.5.07.74a6.55,6.55,0,0,1-.52-4.45,6.86,6.86,0,0,1,1.52-3.17,7,7,0,0,1-.13-3.52C3.86,21.43,6,19.4,8,19.82a2.66,2.66,0,0,1,1.3.69,3.34,3.34,0,0,0-2.83,1.71c-.9,1.84.56,4.39,3.25,5.69l.06,0A6.39,6.39,0,0,1,9,29.05Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M34.05,7.66a7.11,7.11,0,0,1-2.14,2.8,7,7,0,0,1-.6,3.47c-1.19,2.73-3.69,4.28-5.57,3.46a2.88,2.88,0,0,1-1.53-1.69,7.18,7.18,0,0,0,2.87-.42c2.81-1,4.53-3.39,3.84-5.33-.52-1.46-2.27-2.24-4.31-2.1a7.22,7.22,0,0,1,.63-3.16C28.43,2,30.92.4,32.8,1.22S35.24,4.92,34.05,7.66Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M45.21,88.77a7.78,7.78,0,0,0,1.09,1.51,6.54,6.54,0,0,1-1.37.74,7,7,0,0,1-1.55,3.16c-1.91,2.29-4.74,3.08-6.31,1.76s-1.31-4.24.61-6.53a7.07,7.07,0,0,1,2.84-2.08,6.9,6.9,0,0,1,1.55-3.17c1.6-1.92,3.85-2.78,5.46-2.24a2.79,2.79,0,0,0-1.71.32C44,83.24,43.75,86.16,45.21,88.77Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M25.57,78.61l-.28,0a7.07,7.07,0,0,0-3,.22l.05,0A3.3,3.3,0,0,1,25.57,78.61Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M32.41,93.26c-1.69,1.17-4.43.13-6.13-2.33A7,7,0,0,1,25,87.64a6.88,6.88,0,0,1-2.51-2.17,7.1,7.1,0,0,0,1.84.47c2.34.3,4.46-.47,5.38-1.82l0,.25a7,7,0,0,1,2.63,2.34C34.09,89.16,34.1,92.1,32.41,93.26Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M77.1,56a5.12,5.12,0,0,0-.44.63,7.42,7.42,0,0,0-5.88,3.29,6.92,6.92,0,0,1-1.06-3l-.3-.15a6.6,6.6,0,0,0-.6-4.24,7.08,7.08,0,0,0-2.24-2.73,6.2,6.2,0,0,0,0-1.28,2.55,2.55,0,0,1,.54-.49c1.68-1.17,4.43-.13,6.13,2.33a7,7,0,0,1,1.25,3.29A7.07,7.07,0,0,1,77.09,56Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M58.26,88c-2,.57-4.24-1.3-5.06-4.17a7.08,7.08,0,0,1-.13-3.52,7.05,7.05,0,0,1-1.75-3.06c-.82-2.87.11-5.65,2.09-6.22a3.69,3.69,0,0,1,3.77,1.64c-2.27-.46-4.37.24-5,1.82-.78,1.9.83,4.35,3.6,5.48a7.06,7.06,0,0,0,3.48.52,6.38,6.38,0,0,0,1,1l.09.29C61.17,84.62,60.23,87.41,58.26,88Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M78.76,82c-1.92.72-4.32-1-5.37-3.76a7.14,7.14,0,0,1-.41-3.5,4.48,4.48,0,0,1-.5-.48c2.48,0,4.55-1.13,4.84-2.9.33-2-1.79-4-4.74-4.53a7.08,7.08,0,0,0-1.44-.1,2.86,2.86,0,0,1,1.45-1.26c1.92-.72,4.33,1,5.37,3.76a6.92,6.92,0,0,1,.41,3.5,7,7,0,0,1,2,2.91C81.4,78.46,80.68,81.31,78.76,82Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M47.57,68.6a7.16,7.16,0,0,1-2.3,2.67,5.91,5.91,0,0,1-.05,1,7.85,7.85,0,0,0-2.18.58A7.06,7.06,0,0,0,40.23,75a6.86,6.86,0,0,0-3.12.44,6.8,6.8,0,0,1,.73-4.11,7,7,0,0,1,2.3-2.67A6.83,6.83,0,0,1,40.59,66a3.29,3.29,0,0,0,3,.92c1.73-.45,2.71-2.61,2.43-5.07a2.31,2.31,0,0,1,.67.23C48.53,63,48.92,65.94,47.57,68.6Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M65.53,10.56a6.94,6.94,0,0,0,1.68,3.09A6.11,6.11,0,0,0,67,15.1l-.18.07c-2.77,1.12-5.63.49-6.4-1.41S61.28,9.4,64,8.28a7.81,7.81,0,0,1,1.28-.39A7.46,7.46,0,0,0,65.53,10.56Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M73.17,12.61h0a3.7,3.7,0,0,0-.37-.42,6.91,6.91,0,0,0,0-3.52A7,7,0,0,0,71,5.51c2.57-.81,5.09-.14,5.81,1.63S75.94,11.49,73.17,12.61Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M49.7,10.19h0a5.43,5.43,0,0,0-.23,1,7.11,7.11,0,0,0-2.72,2.24,7,7,0,0,0-1.36,3.11,7.65,7.65,0,0,1-1.17-1.34,7.38,7.38,0,0,1-.83-1.53c.52-2.53-.34-4.86-2.09-5.43a2.78,2.78,0,0,0-1.71,0C38.67,6.16,38.91,4,40.3,3c1.69-1.17,4.44-.12,6.14,2.33a7.15,7.15,0,0,1,1.25,3.29A6.84,6.84,0,0,1,49.7,10.19Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M41.24,27.86a2.85,2.85,0,0,1-1.43,1.65,5.91,5.91,0,0,0-5.37,0l0,0A7,7,0,0,1,32,27.91a7,7,0,0,1-3.51-.24c-2.85-.91-4.64-3.23-4-5.19s3.44-2.8,6.28-1.89a7,7,0,0,1,3,1.84h.06a3.4,3.4,0,0,0,2.09,2.63,3.56,3.56,0,0,0,3.58-1.24C40.92,24.93,41.68,26.49,41.24,27.86Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M18.4,58.38a2.62,2.62,0,0,1-.74,0,2.64,2.64,0,0,0,.65-1.25c.43-2-1.53-4.09-4.38-4.75A6.32,6.32,0,0,1,14.37,50a7,7,0,0,1-1.3-3.27c-.33-2.43.51-4.64,1.94-5.49a6.85,6.85,0,0,0,2.46,1.89,7,7,0,0,0,1.39,3.24,7.16,7.16,0,0,0,1.56,1.55,6.44,6.44,0,0,1-.36,1.31,7,7,0,0,1,1.3,3.28C21.76,55.49,20.44,58.11,18.4,58.38Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M69.41,36c-2.19-.66-4.31-.17-5.11,1.32a3.09,3.09,0,0,0,0,2.63H64a7,7,0,0,1-3.42-.84,7,7,0,0,1-3.42.84,6.4,6.4,0,0,1-4.42-1.57,3.28,3.28,0,0,1-.7-.95,7.6,7.6,0,0,1-2-1.1A7.13,7.13,0,0,1,48,33.85a2.3,2.3,0,0,1-.15-.32,7,7,0,0,1-3.23-1.41c-2.37-1.82-3.28-4.61-2-6.24s4.18-1.48,6.55.34A7,7,0,0,1,51.35,29a6,6,0,0,1,1.3.34,7.53,7.53,0,0,1,1.31.64h0c.21.13.42.28.62.43a7.55,7.55,0,0,1,1.9,2.13c.23,0,.46,0,.7,0l.49,0a6.77,6.77,0,0,1,1,.13h0a6.6,6.6,0,0,1,1.9.69A7.06,7.06,0,0,1,64,32.47C66.89,32.47,69.24,34,69.41,36Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M37.48,36.32A7,7,0,0,1,34,37a7,7,0,0,1-2.78,2.17c-2.72,1.22-5.61.69-6.45-1.19s.69-4.38,3.42-5.6a7.1,7.1,0,0,1,3.46-.63,7,7,0,0,1,2.74-2.14l0,0a5.91,5.91,0,0,1,5.37,0,2.74,2.74,0,0,1,1.08,1.21C41.73,32.59,40.2,35.1,37.48,36.32Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M25.08,48.48c-1.22.91-3.06.63-4.66-.54a7.16,7.16,0,0,1-1.56-1.55,7,7,0,0,1-1.39-3.24A6.85,6.85,0,0,1,15,41.26c-.09-.11-.18-.22-.26-.34-1.8-2.38-1.92-5.32-.28-6.55s4.42-.3,6.22,2.09a7,7,0,0,1,1.38,3.23,7.1,7.1,0,0,1,2.73,2.23C26.59,44.31,26.72,47.24,25.08,48.48Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M27.08,15.28a7.18,7.18,0,0,1-2.87.42,6.12,6.12,0,0,1-.63-.06,7.07,7.07,0,0,1-2.94,1.94c-2.81,1-5.65.25-6.34-1.68s1-4.31,3.84-5.32a7,7,0,0,1,3.5-.36,7,7,0,0,1,2.94-1.94,7.31,7.31,0,0,1,2-.43c2-.14,3.79.64,4.31,2.1C31.61,11.89,29.89,14.27,27.08,15.28Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M22.35,29.89c-.89,1.85-3.79,2.29-6.48,1a7.07,7.07,0,0,1-2.72-2.25,7,7,0,0,1-3.38-.7l-.06,0C7,26.61,5.56,24.06,6.46,22.22a3.34,3.34,0,0,1,2.83-1.71,6.87,6.87,0,0,1,3.65.71,7,7,0,0,1,2.71,2.24,7.17,7.17,0,0,1,3.45.73C21.79,25.49,23.24,28,22.35,29.89Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M6.63,46.81c-2,.1-3.83-2.23-4-5.21a7.06,7.06,0,0,1,.67-3.46,6.9,6.9,0,0,1-.94-2.64c0-.24,0-.49-.07-.74-.15-3,1.39-5.47,3.44-5.58,1.46-.07,2.78,1.09,3.48,2.83a7.52,7.52,0,0,1,.5,2.39,7,7,0,0,1-.66,3.45,7.11,7.11,0,0,1,1,3.37C10.22,44.21,8.68,46.71,6.63,46.81Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M56.8,12.42a7.06,7.06,0,0,1-2.72,2.23,7,7,0,0,1-1.38,3.24c-1.79,2.39-4.57,3.33-6.21,2.1a3.48,3.48,0,0,1-1.1-3.44,7,7,0,0,1,1.36-3.11,7.11,7.11,0,0,1,2.72-2.24,5.43,5.43,0,0,1,.23-1h0A7.3,7.3,0,0,1,50.85,8c1.79-2.39,4.57-3.33,6.21-2.1S58.59,10,56.8,12.42Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M43.39,13.68c-.06.29-.13.57-.22.86a7,7,0,0,1-1.86,3,7.37,7.37,0,0,1-1.87,6.27,3.56,3.56,0,0,1-3.58,1.24,3.4,3.4,0,0,1-2.09-2.63h0A6.94,6.94,0,0,1,34,18.76a7.1,7.1,0,0,1,1.85-3,7,7,0,0,1,.26-3.51,6,6,0,0,1,3.49-4,2.78,2.78,0,0,1,1.71,0C43.05,8.82,43.91,11.15,43.39,13.68Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M79.87,45.58c-1,1.81-3.88,2.14-6.52.73A7.1,7.1,0,0,1,70.73,44a7,7,0,0,1-3.41-.87,6.59,6.59,0,0,1-3.06-3.19,3.09,3.09,0,0,1,0-2.63c.8-1.49,2.92-2,5.11-1.32a8.39,8.39,0,0,1,1.41.58,7.18,7.18,0,0,1,2.62,2.35,7.06,7.06,0,0,1,3.41.87C79.49,41.16,80.84,43.77,79.87,45.58Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M83.34,30.22a7.32,7.32,0,0,1-1.8,1.09,7,7,0,0,1-3.48.6A7,7,0,0,1,75.26,34c-2.74,1.19-5.62.62-6.44-1.26s.74-4.37,3.48-5.56a7.1,7.1,0,0,1,3.47-.59,7.13,7.13,0,0,1,2.81-2.13q.32-.13.63-.24c2.54-.85,5.05-.24,5.81,1.5C85.63,27.17,84.9,28.93,83.34,30.22Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M69.42,56.75A3.11,3.11,0,0,1,67.8,59c-1.85.89-4.39-.57-5.68-3.26a7.05,7.05,0,0,1-.73-3.45,7,7,0,0,1-2.23-2.72c-1.29-2.69-.84-5.59,1-6.48s4.4.58,5.69,3.27a7.57,7.57,0,0,1,.67,2.16,6.2,6.2,0,0,1,0,1.28,7.08,7.08,0,0,1,2.24,2.73A6.6,6.6,0,0,1,69.42,56.75Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M82.9,60.64a7,7,0,0,1-3.39,1A7.05,7.05,0,0,1,77,64c-2.59,1.48-5.52,1.24-6.53-.55a3.47,3.47,0,0,1,.36-3.58,7.42,7.42,0,0,1,5.88-3.29A5.12,5.12,0,0,1,77.1,56a7.18,7.18,0,0,1,2.11-1.79c2.6-1.48,5.52-1.24,6.54.55S85.49,59.16,82.9,60.64Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M90.94,50.48a2.8,2.8,0,0,1-1.59.34A5.94,5.94,0,0,1,85,47.62a7,7,0,0,1-1-3.38,7.07,7.07,0,0,1-2.43-2.56c-1.47-2.59-1.23-5.52.55-6.54s4.43.27,5.91,2.86a7.13,7.13,0,0,1,1,3.39,7,7,0,0,1,2.42,2.55A7.47,7.47,0,0,1,92,45.13C92.86,47.37,92.45,49.61,90.94,50.48Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M93.59,65.59a7.11,7.11,0,0,1-2.92,2A7,7,0,0,1,89,70.67c-2,2.22-4.85,2.9-6.38,1.52s-1.13-4.28.87-6.5a7,7,0,0,1,2.91-2,7.07,7.07,0,0,1,1.67-3.1c.15-.16.31-.33.47-.47,1.95-1.84,4.5-2.32,5.91-1a3.07,3.07,0,0,1,.25.26C95.92,60.81,95.47,63.51,93.59,65.59Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M73.53,91a7.07,7.07,0,0,1-3.3,1.23,7.09,7.09,0,0,1-2.36,2.62c-2.47,1.68-5.4,1.67-6.56,0s-.09-4.43,2.38-6.11A7.12,7.12,0,0,1,67,87.45a7.13,7.13,0,0,1,2.35-2.62c2.47-1.67,5.41-1.66,6.56,0S76,89.3,73.53,91Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M77.32,71.38c-.29,1.77-2.36,3-4.84,2.9a8.48,8.48,0,0,1-1.1-.1,7,7,0,0,1-3.24-1.38,7,7,0,0,1-3.51.28A7.33,7.33,0,0,1,61.79,72,5.79,5.79,0,0,1,57,69.27,7.17,7.17,0,0,1,55.68,66,7,7,0,0,1,53,63.71a7.37,7.37,0,0,1-1-1.81c-1.51-.61-2.73-2.52-2.94-4.87a7.09,7.09,0,0,1,.54-3.48,6.91,6.91,0,0,1-1.14-3.33c-.27-3,1.17-5.53,3.22-5.71s3.91,2.08,4.18,5.05A7.09,7.09,0,0,1,55.38,53a7.13,7.13,0,0,1,1.14,3.33q0,.41,0,.81a7.39,7.39,0,0,1,3.81,5.47A7,7,0,0,1,63,64.94a6.69,6.69,0,0,1,.49.78,7.47,7.47,0,0,1,2.31,0,7,7,0,0,1,3.24,1.38,6.46,6.46,0,0,1,2.07-.37,7.08,7.08,0,0,1,1.44.1C75.53,67.33,77.65,69.36,77.32,71.38Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M35.63,69.89c-.59,1.51-2.48,2.25-4.63,2a7.28,7.28,0,0,1-1.75-.45,6.91,6.91,0,0,1-2.89-2,7.05,7.05,0,0,1-3.5-.44,7.61,7.61,0,0,1-2.14-1.26c-1.42-1.21-2.08-2.81-1.57-4.15.74-1.92,3.6-2.6,6.38-1.53a7.2,7.2,0,0,1,2.9,2,7,7,0,0,1,3.49.45C34.7,65.55,36.36,68,35.63,69.89Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M18.31,57.13a2.64,2.64,0,0,1-.65,1.25c-1.06,1.18-3.18,1.72-5.42,1.24a7,7,0,0,1-3.16-1.54,7.14,7.14,0,0,1-3.52.1c-2.92-.63-4.94-2.77-4.5-4.78s3.15-3.12,6.07-2.49a7.11,7.11,0,0,1,3.16,1.55,6.91,6.91,0,0,1,3.52-.1l.12,0C16.78,53,18.74,55.15,18.31,57.13Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M72.22,21.47c-2,.51-4.2-1.41-5-4.29A7.67,7.67,0,0,1,67,15.1a6.11,6.11,0,0,1,.2-1.45,6.94,6.94,0,0,1-1.68-3.09,7.46,7.46,0,0,1-.21-2.67c.19-1.76,1.09-3.15,2.44-3.5A3.41,3.41,0,0,1,71,5.51a7,7,0,0,1,1.74,3.17,6.91,6.91,0,0,1,0,3.52,3.7,3.7,0,0,1,.37.42,7.15,7.15,0,0,1,1.31,2.68C75.2,18.18,74.21,21,72.22,21.47Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M43.6,66.94a3.29,3.29,0,0,1-3-.92,6.87,6.87,0,0,1-2-3.37,7,7,0,0,1-.06-3.52A7.06,7.06,0,0,1,36.91,56c-.76-2.88.24-5.64,2.23-6.16s4.2,1.4,5,4.29a7.13,7.13,0,0,1,0,3.52,7.09,7.09,0,0,1,1.68,3.09,8.8,8.8,0,0,1,.2,1.1C46.31,64.33,45.33,66.49,43.6,66.94Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M30.18,82.94a2.62,2.62,0,0,1-.45,1.18c-.92,1.35-3,2.12-5.38,1.82a7.1,7.1,0,0,1-1.84-.47,6,6,0,0,1-1.44-.79,7.14,7.14,0,0,1-3.51.4c-2.95-.38-5.15-2.34-4.88-4.37s2.87-3.38,5.83-3A7,7,0,0,1,21.79,79a4.72,4.72,0,0,1,.53-.18,7.07,7.07,0,0,1,3-.22l.28,0C28.39,79.07,30.43,81,30.18,82.94Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M68.51,81.13c-.78,1.9-3.65,2.53-6.41,1.4a7.86,7.86,0,0,1-1.84-1.07,6.38,6.38,0,0,1-1-1,7.06,7.06,0,0,1-3.48-.52c-2.77-1.13-4.38-3.58-3.6-5.48.64-1.58,2.74-2.28,5-1.82a7.45,7.45,0,0,1,1.4.42,7.05,7.05,0,0,1,2.85,2.07,7.08,7.08,0,0,1,3.48.52C67.67,76.78,69.28,79.23,68.51,81.13Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M46,79.7a7,7,0,0,1-3.48.58,6.88,6.88,0,0,1-2.81,2.12c-2.74,1.19-5.62.62-6.43-1.27s.75-4.37,3.49-5.55l.36-.14A6.86,6.86,0,0,1,40.23,75,7.06,7.06,0,0,1,43,72.87a7.85,7.85,0,0,1,2.18-.58c2-.2,3.66.48,4.25,1.86C50.28,76,48.72,78.52,46,79.7Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M54.41,97.66c-1.79,1-4.42-.31-5.87-2.92a7,7,0,0,1-.93-3.39,6.48,6.48,0,0,1-1.31-1.07,7.78,7.78,0,0,1-1.09-1.51c-1.46-2.61-1.18-5.53.61-6.53a2.79,2.79,0,0,1,1.71-.32,6,6,0,0,1,4.17,3.23,7,7,0,0,1,.93,3.4A6.92,6.92,0,0,1,55,91.13C56.48,93.73,56.21,96.66,54.41,97.66Z"/>
                        <path stroke=${pathColor} class="main-po-cls" d="M13.4,67.55a7,7,0,0,1-1.83,3,7.05,7.05,0,0,1-.23,3.52c-.9,2.84-3.22,4.65-5.17,4s-2.82-3.43-1.92-6.28a7.19,7.19,0,0,1,1.84-3,6.91,6.91,0,0,1,.23-3.51c.9-2.85,3.22-4.65,5.17-4S14.3,64.71,13.4,67.55Z"/>
                        </g>
                        </g>
                    </svg>
                </div>
            `);
        case 'Glüten Hassasiyetin':
            return (`
                <div class='gs-icon-container'>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 174.28 156.11" width='${hxSize}' height='${hxSize}'>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="_1" data-name="1">
                        <path fill=${hexColor} d="M121,0H53.25A19.34,19.34,0,0,0,36.49,9.68L2.59,68.38a19.38,19.38,0,0,0,0,19.35l33.9,58.71a19.33,19.33,0,0,0,16.76,9.67H121a19.34,19.34,0,0,0,16.76-9.67l33.89-58.71a19.33,19.33,0,0,0,0-19.35L137.79,9.68A19.35,19.35,0,0,0,121,0Z"/>
                        </g>
                        </g>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 51.56 89.27" width='${gsSize}' height='${gsSize}' style='position: absolute; left: calc(50% - ${gsSize/2}px); top: calc(50% - ${gsSize/2}px)'>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="_1" data-name="1">
                        <path fill=${pathColor} d="M31.79,68.24,31.56,68l-.5.08a3.07,3.07,0,0,0,.28-.35l-1.58-1.89a11.92,11.92,0,0,1-1.18,1.41c-2.8,2.8-6.86,3.69-8.95,4,.28-2.08,1.17-6.14,4-8.94a11,11,0,0,1,1.91-1.53l-.93-1.11-.89-1.08a1.7,1.7,0,0,1-.17-.23,19.15,19.15,0,0,1-3.89.93c.28-2.09,1.17-6.15,4-8.95a13.46,13.46,0,0,1,5.25-3.11V43c-.08.1-.18.2-.27.3-2.8,2.79-6.86,3.68-8.95,4,.28-2.09,1.17-6.16,4-9a13.46,13.46,0,0,1,5.25-3.11V32.67a17.67,17.67,0,0,0-5,2.3,13.54,13.54,0,0,0,.39-3.16c0-5.48-3.63-10-4.79-11.24V18.64a1.2,1.2,0,1,0-2.39,0v1.93c-1.15,1.28-4.79,5.76-4.79,11.24a13,13,0,0,0,.4,3.16,20.07,20.07,0,0,0-8.61-3.06l-.84-.07L2,30.61a.58.58,0,0,0-.15-.11,1.11,1.11,0,0,0-.69-.24,1.22,1.22,0,0,0-.85.35,1.2,1.2,0,0,0,0,1.7L1,33l.56.57a23,23,0,0,0,.62,3.71h0a17.42,17.42,0,0,0,3.37,6.86,21.26,21.26,0,0,0-2.29-.3L2,42.59a1.14,1.14,0,0,0-.84-.35,1.2,1.2,0,0,0-.85,2L1.58,45.5a19.37,19.37,0,0,0,4,10.58,18.7,18.7,0,0,0-2.29-.3L2.05,54.55a1.24,1.24,0,0,0-.85-.34,1.26,1.26,0,0,0-.85.34,1.22,1.22,0,0,0,0,1.7l1.23,1.22a19.37,19.37,0,0,0,4,10.58,21.26,21.26,0,0,0-2.29-.3L2,66.53a1.16,1.16,0,0,0-.84-.35,1.2,1.2,0,0,0-.85,2l1.22,1.22c.13,1.71.82,7.49,4.78,11.44,3.53,3.54,8.52,4.47,10.77,4.71v3.68h2.39V85.59c2.24-.25,7.23-1.17,10.76-4.71a14.4,14.4,0,0,0,1.64-2,19.21,19.21,0,0,0,2.83-7.13ZM21.12,61.41A15.24,15.24,0,0,0,19.5,63.8V61.64A16,16,0,0,0,21.12,61.41Zm0-12a15.64,15.64,0,0,0-1.62,2.38V49.68C20,49.62,20.5,49.55,21.12,49.44Zm-17-15.12c2.08.29,6.14,1.17,8.94,4s3.7,6.86,4,9c-2.08-.28-6.13-1.16-8.94-4S4.34,36.41,4.07,34.32Zm0,12c2.08.27,6.14,1.16,8.94,4s3.7,6.86,4,8.95c-2.08-.28-6.13-1.16-8.94-4S4.34,48.39,4.07,46.3Zm0,12c2.08.28,6.14,1.16,8.94,4s3.7,6.86,4,8.95c-2.08-.27-6.13-1.16-8.94-4S4.34,60.36,4.07,58.27ZM8,79.19c-2.8-2.8-3.7-6.86-4-9,2.08.27,6.14,1.16,8.94,4s3.7,6.87,4,9C14.9,82.88,10.85,82,8,79.19Zm9.07-3.42a15.17,15.17,0,0,0-1.61-2.39c.62.11,1.17.19,1.61.24Zm0-12a15.17,15.17,0,0,0-1.61-2.39c.62.12,1.17.19,1.61.23Zm0-12a15.57,15.57,0,0,0-1.61-2.38c.62.11,1.17.19,1.61.24Zm-2.4-20c0-3.89,2.3-7.32,3.59-8.94,1.3,1.62,3.6,5.05,3.6,8.94s-2.3,7.32-3.6,9C17,39.13,14.71,35.69,14.71,31.81Zm4.79,44V73.62c.45-.05,1-.13,1.62-.24A15.24,15.24,0,0,0,19.5,75.77Zm9.08,3.42c-2.8,2.79-6.86,3.69-8.95,4,.28-2.09,1.17-6.15,4-9s6.86-3.69,8.95-4C32.27,72.34,31.37,76.39,28.58,79.19Z" transform="translate(0)"/>
                        <path fill=${pathColor} d="M51.28,57l-12,14.37a1.24,1.24,0,0,1-.92.43,1.21,1.21,0,0,1-.92-.44l-2-2.39-1.54-1.85-.34-.41-.91-1.09-1.67-2L28.79,61l-1.16-1.39L26.39,58.1l-.65-.78L25.5,57a1.2,1.2,0,0,1,.89-2H31.2V19.15a1.2,1.2,0,0,1,2.4,0V56.26a1.2,1.2,0,0,1-1.2,1.2H29l1.1,1.31,1.12,1.36.78.93L33.6,63l.18.22,4.61,5.53,9.41-11.3H44.37a1.2,1.2,0,0,1-1.19-1.2V18a1.2,1.2,0,0,1,2.39,0V55.06h4.79a1.2,1.2,0,0,1,1.08.69A1.22,1.22,0,0,1,51.28,57Z" transform="translate(0)"/>
                        <path fill=${pathColor} d="M45.57,13.16A1.2,1.2,0,1,1,44.37,12,1.19,1.19,0,0,1,45.57,13.16Z" transform="translate(0)"/>
                        <path fill=${pathColor} d="M45.57,1.19V8.37a1.2,1.2,0,1,1-2.39,0V1.19a1.2,1.2,0,0,1,2.39,0Z" transform="translate(0)"/>
                        <path fill=${pathColor} d="M40.78,23.94a1.2,1.2,0,1,1-1.2-1.2A1.2,1.2,0,0,1,40.78,23.94Z" transform="translate(0)"/>
                        <circle fill=${pathColor} cx="39.58" cy="19.15" r="1.2"/>
                        <path fill=${pathColor} d="M40.78,14.36a1.2,1.2,0,1,1-1.2-1.2A1.19,1.19,0,0,1,40.78,14.36Z" transform="translate(0)"/>
                        <path fill=${pathColor} d="M40.78,28.73V39.5H38.39V28.73a1.2,1.2,0,1,1,2.39,0Z" transform="translate(0)"/>
                        <path fill=${pathColor} d="M33.6,9.57v4.79a1.2,1.2,0,0,1-2.4,0V9.57a1.2,1.2,0,0,1,2.4,0Z" transform="translate(0)"/>
                        <path fill=${pathColor} d="M31.2,4.78A1.2,1.2,0,1,1,32.4,6,1.2,1.2,0,0,1,31.2,4.78Z" transform="translate(0)"/>
                        </g>
                        </g>
                    </svg>
                </div>
            `);
        case 'Antibiyotik Hasarın':
            return (`
                <div class='gs-icon-container'>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 174.28 156.11" width='${hxSize}' height='${hxSize}'>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="_1" data-name="1">
                        <path fill=${hexColor} d="M121,0H53.25A19.34,19.34,0,0,0,36.49,9.68L2.59,68.38a19.38,19.38,0,0,0,0,19.35l33.9,58.71a19.33,19.33,0,0,0,16.76,9.67H121a19.34,19.34,0,0,0,16.76-9.67l33.89-58.71a19.33,19.33,0,0,0,0-19.35L137.79,9.68A19.35,19.35,0,0,0,121,0Z"/>
                        </g>
                        </g>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60.17 77.77" width='${gsSize}' height='${gsSize}' style='position: absolute; left: calc(50% - ${gsSize/2}px); top: calc(50% - ${gsSize/2}px)'>
                        <g id="Layer_2" data-name="Layer 2">
                        <g id="_1" data-name="1">
                        <path fill=${pathColor} d="M36.42,54.14a10.22,10.22,0,0,0-1.93,2.7H39v-2.7ZM42,14.86a5.45,5.45,0,0,0,2.2-4.38v-5A5.48,5.48,0,0,0,38.69,0H7.46A5.46,5.46,0,0,0,2,5.46v5a5.44,5.44,0,0,0,2.2,4.39A8.5,8.5,0,0,0,0,22.08V58.24a8,8,0,0,0,2.11,5.35,7.44,7.44,0,0,0,5.52,2.07H24.91A17.24,17.24,0,0,1,24.5,63H7.63A4.85,4.85,0,0,1,4,61.71a5.47,5.47,0,0,1-1.35-3.45V56.84H25a17.72,17.72,0,0,1,.87-2.7H2.69V25.37H43.45V42.05h.35a18.31,18.31,0,0,1,2.35.15V22.08A8.5,8.5,0,0,0,42,14.86ZM4.69,5.46A2.78,2.78,0,0,1,7.46,2.69H38.69a2.77,2.77,0,0,1,2.76,2.77v5a2.76,2.76,0,0,1-2.76,2.77H7.46a2.78,2.78,0,0,1-2.77-2.77ZM43.45,22.67H2.69v-.59C2.69,17.9,7.34,16.18,8,16H38.19c.69.24,5.26,2,5.26,6.13ZM39,63.83v-.89a3.46,3.46,0,0,1-.46,0h-5a10.26,10.26,0,0,0,.79,2.7h4.18c.28,0,.54,0,.81,0A4.69,4.69,0,0,1,39,63.83Z"/>
                        <path fill=${pathColor} d="M39.12,4.88v6.5a1.35,1.35,0,1,1-2.7,0V4.88a1.35,1.35,0,0,1,2.7,0Z"/>
                        <path fill=${pathColor} d="M34.92,4.88v6.5a1.35,1.35,0,1,1-2.7,0V4.88a1.35,1.35,0,0,1,2.7,0Z"/>
                        <path fill=${pathColor} d="M30.72,4.88v6.5a1.35,1.35,0,1,1-2.69,0V4.88a1.35,1.35,0,0,1,2.69,0Z"/>
                        <path fill=${pathColor} d="M26.53,4.88v6.5a1.35,1.35,0,1,1-2.7,0V4.88a1.35,1.35,0,0,1,2.7,0Z"/>
                        <path fill=${pathColor} d="M22.33,4.88v6.5a1.35,1.35,0,1,1-2.7,0V4.88a1.35,1.35,0,0,1,2.7,0Z"/>
                        <path fill=${pathColor} d="M18.13,4.88v6.5a1.35,1.35,0,1,1-2.7,0V4.88a1.35,1.35,0,0,1,2.7,0Z"/>
                        <path fill=${pathColor} d="M13.93,4.88v6.5a1.36,1.36,0,1,1-2.71,0V4.88a1.36,1.36,0,0,1,2.71,0Z"/>
                        <path fill=${pathColor} d="M9.73,4.88v6.5a1.35,1.35,0,1,1-2.7,0V4.88a1.35,1.35,0,0,1,2.7,0Z"/>
                        <path fill=${pathColor} d="M12.32,32A4.24,4.24,0,0,0,8.1,36.27v6.39a4.23,4.23,0,1,0,8.45,0V36.27A4.24,4.24,0,0,0,12.32,32Zm1.53,10.62a1.53,1.53,0,1,1-3.06,0V40.82h3.06Zm0-4.54H10.79V36.27a1.53,1.53,0,1,1,3.06,0Z"/>
                        <path fill=${pathColor} d="M34,32a4.24,4.24,0,0,0-4.23,4.23v6.39a4.23,4.23,0,0,0,2,3.6,19.31,19.31,0,0,1,6.44-3.39,1.48,1.48,0,0,0,0-.21V36.27A4.24,4.24,0,0,0,34,32Zm1.53,10.62a1.53,1.53,0,0,1-3.06,0V40.82h3.06Zm0-4.54H32.43V36.27a1.53,1.53,0,0,1,3.06,0Z"/>
                        <path fill=${pathColor} d="M23.14,32a4.24,4.24,0,0,0-4.23,4.23v6.39a4.23,4.23,0,0,0,8.46,0V36.27A4.23,4.23,0,0,0,23.14,32Zm1.53,10.62a1.53,1.53,0,0,1-3.06,0V40.82h3.06Zm0-4.54H21.61V36.27a1.53,1.53,0,0,1,3.06,0Z"/>
                        <path fill=${pathColor} d="M43.8,48A13.37,13.37,0,1,1,30.43,61.4,13.37,13.37,0,0,1,43.8,48Zm0,17.62a1.82,1.82,0,0,0,1.82-1.83V52.9a1.82,1.82,0,1,0-3.64,0V63.83a1.82,1.82,0,0,0,1.82,1.83Zm0,6.07h0A1.82,1.82,0,1,0,42,69.91a1.81,1.81,0,0,0,1.83,1.82ZM43.8,45h0A16.37,16.37,0,1,0,60.17,61.4,16.39,16.39,0,0,0,43.8,45Z"/>
                        </g>
                        </g>
                    </svg>
                </div>
            `);
        default:
            return (`
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 174.28 156.11" width='${hxSize}' height='${hxSize}'>
                    <g id="Layer_2" data-name="Layer 2">
                    <g id="_1" data-name="1">
                    <path class='mt-primary' d="M121,0H53.25A19.34,19.34,0,0,0,36.49,9.68L2.59,68.38a19.38,19.38,0,0,0,0,19.35l33.9,58.71a19.33,19.33,0,0,0,16.76,9.67H121a19.34,19.34,0,0,0,16.76-9.67l33.89-58.71a19.33,19.33,0,0,0,0-19.35L137.79,9.68A19.35,19.35,0,0,0,121,0Z"/>
                    </g>
                    </g>
                </svg>
            `)
    }
}


// HEALTH PDF HELPERS
hbs.registerHelper('printDiseases', (browser, req, isSecret, kitCode, registerDate, diseases) => {
    return (async () => {
        try{
            const testPage = await browser.newPage();
            const testContent = await this.compile('test', {
                req, 
                kitCode, 
                registerDate
            });
            await testPage.setContent(testContent, {waitUntil: 'load'})

            await testPage.evaluate(() => {
                window.getContentHeight = view => {
                    const viewHtml = document.querySelector('#content');
                    viewHtml.innerHTML = view;
                    const height = viewHtml.clientHeight;
                    viewHtml.innerHTML = '';
                    console.log(height);
                    return height;
                }
            })

            await testPage.evaluate(() => {
                window.chunkArray = (arr, chunkItems) => (
                    arr.reduce((acc, currentValue, index) => {
                        const chunkIndex = Math.floor(index/chunkItems);
                
                        if(!acc[chunkIndex]){ 
                            acc[chunkIndex] = []
                        }
                
                        acc[chunkIndex].push(currentValue);
                
                        return acc;
                    }, [])
                )
            })
            /***************************************/

            const trans = {
                report: req.__('p-head-l-0'),
                report_title: req.__('m-report-title'),
                user: req.__('p-head-l-1'),
                date: req.__('p-head-l-2')
            }

            return await testPage.evaluate((trans, isSecret, kitCode, registerDate, diseases, status_colors) => {
                const totalHeight = 1100;
                let heightSum = 0;
                const startView = `
                    <div class='page portrait'>
                        <div class='page-header'>
                            <div class='report-name'>
                                <label class='t-bold secondary'>${trans.report}</label>
                                <div>${trans.report_title}</div>
                            </div>
                            <div class='report-kitCode'>
                                <label class='t-bold secondary'>${trans.user}</label>
                                <div>
                                    ${isSecret ? '.....' : `${kitCode}`}
                                </div>
                            </div>
                            <div>
                                <label class='t-bold secondary'>${trans.date}</label>
                                <div>${registerDate}</div>
                            </div>
                        </div>
                        <hr class='line-breaker'>
                `;
                const endView = `
                        <div class='page-footer'>
                            <img class='footer-img' src='https://enbiosis-pdf-generator.herokuapp.com/img/logo-2x.png' alt='enbiosis-logo'/>
                        </div>
                    </div>
                `;
                const printDesc = disease => (`
                    <div class='main-title t-bold t-margin primary' id='title'>
                        ${disease.name} 
                    </div>
                    ${disease.desc.map(d => (
                        `
                            ${d.title !== '' ? 
                                `
                                <div class='alt-title t-bold t-margin primary'>
                                    ${d.title}
                                </div>
                                ` : ''
                            }
                            <div class='t-margin'>
                                ${d.content}
                            </div>
                        `
                    )).join('')}
                `)
                const printDiseaseTable = disease => (`
                    <div class='dis-container t-margin' id='table'>
                        <div class='dis-header primary t-bold border-b'>
                            ${disease.name}
                        </div>
                        ${!disease.attrs.is_array ? 
                            Object.keys(disease.attrs.items).map((attr, aIndex, arr) => 
                                `
                                    <div class='dis-attr dis-header ${aIndex !== arr.length - 1 ? 'border-b' : ''}'>
                                        <div class='dis-alt'>
                                            ${attr}
                                        </div>
                                        <div class='dis-alt dis-status'>
                                            ${disease.attrs.items[attr] ? 'var' : 'yok'}
                                            <i 
                                                class="fas fa-minus-circle" 
                                                style="
                                                    color: ${disease.attrs.items[attr] === disease.attrs.isPositive ? '#83c341' : '#d0021b'}
                                                "
                                            ></i>
                                        </div>
                                    </div>
                                `
                            ).join('') : 
                            disease.attrs.items.map((item, aIndex, arr) => 
                                item[Object.keys(item)[0]].length !== 0 ? 
                                    `
                                        <div class='dis-attr ${aIndex !== arr.length - 1 ? 'border-b' : ''}'>
                                            <div class='dis dis-header'>
                                                ${Object.keys(item)[0]}
                                            </div>
                                            <div class='attr-wrapper'>
                                                ${item[Object.keys(item)[0]].map((altAttr, alIndex, arr) => 
                                                    `
                                                        <div class='dis-attr attr-p ${alIndex !== arr.length - 1 ? 'border-b' : ''}'>
                                                            <div class='attr-name'>
                                                                ${altAttr.title} 
                                                                ${altAttr.refs.length !== 0 ? 
                                                                    `
                                                                        <div class='links-wrapper'>
                                                                            [${altAttr.refs.map((ref, index, arr) => 
                                                                                `<a class='ref-link primary t-bold' href='#${ref}'>${ref}</a>${index !== arr.length - 1 ? ', ' : ''}`
                                                                            ).join('')}]
                                                                        </div>
                                                                    ` : ''
                                                                }
                                                            </div>
                                                            ${altAttr.isBoolean !== undefined ? 
                                                                `
                                                                    <div class='dis-status-alt'>
                                                                        <i 
                                                                            class="fas fa-minus-circle" 
                                                                            style="
                                                                                margin-right: 20px;
                                                                                color: ${disease.attrs.isPositive === altAttr.isBoolean ? '#83c341' : '#d0021b'}
                                                                            "
                                                                        ></i>
                                                                        ${altAttr.isBoolean ? 'var' : 'yok'}
                                                                    </div>
                                                                ` : 
                                                                `
                                                                    <div class='attr-value'>
                                                                        <div class='rank'>
                                                                            <span style='${altAttr.rank === 'high' ? `background-color: ${status_colors[altAttr.rank]}` : ''}'></span>
                                                                            <span style='${altAttr.rank === 'mid' || altAttr.rank === 'high' ? `background-color: ${status_colors[altAttr.rank]}` : ''}'></span>
                                                                            <span style='background-color: ${status_colors[altAttr.rank]}'></span>
                                                                        </div>
                                                                        ${altAttr.rankTitle}
                                                                    </div>
                                                                `
                                                            }
                                                        </div>
                                                    `
                                                ).join('')}
                                            </div>
                                        </div>
                                    ` : null
                            ).join('')
                        }
                    </div>
                `)

                const printDisease = disease => (`
                    ${printDesc(disease)}
                    ${printDiseaseTable(disease)}
                `)

                const printNormalDisease = disease => {
                    const modifiedAttrs = chunkArray(Object.keys(disease.attrs.items), 18);
                    return modifiedAttrs.map((mAttr, maIndex) => 
                        `
                            ${maIndex !== 0 ? startView : ''}

                                ${maIndex === 0 ? 
                                    `
                                        ${printDesc(disease)}
                                        <div class='dis-container t-margin'>
                                            <div class='dis-header primary t-bold border-b'>
                                                ${disease.name}
                                            </div>
                                            ${mAttr.map((attr, aIndex, arr) => 
                                                `
                                                    <div class='dis-attr dis-header ${aIndex !== arr.length - 1 ? 'border-b' : ''}'>
                                                        <div class='dis-alt'>
                                                            ${attr}
                                                        </div>
                                                        <div class='dis-alt dis-status'>
                                                            ${disease.attrs.items[attr] ? 'var' : 'yok'}
                                                            <i 
                                                                class="fas fa-minus-circle" 
                                                                style="
                                                                    color: ${disease.attrs.items[attr] === disease.attrs.isPositive ? '#83c341' : '#d0021b'}
                                                                "
                                                            ></i>
                                                        </div>
                                                    </div>
                                                `
                                            ).join('')}
                                        </div>
                                    ` : 
                                    `
                                        <div class='dis-container t-margin'>
                                            ${mAttr.map((attr, aIndex, arr) => 
                                                `
                                                    <div class='dis-attr dis-header ${aIndex !== arr.length - 1 ? 'border-b' : ''}'>
                                                        <div class='dis-alt'>
                                                            ${attr}
                                                        </div>
                                                        <div class='dis-alt dis-status'>
                                                            ${disease.attrs.items[attr] ? 'var' : 'yok'}
                                                            <i 
                                                                class="fas fa-minus-circle" 
                                                                style="
                                                                    color: ${disease.attrs.items[attr] === disease.attrs.isPositive ? '#83c341' : '#d0021b'}
                                                                "
                                                            ></i>
                                                        </div>
                                                    </div>
                                                `
                                            ).join('')}
                                        </div>
                                    `
                                }
                                
                            ${endView}
                        `
                    ).join('');
                }

                const printComplexDisease = disease => {
                    const newAttrs = disease.attrs.items.reduce((acc, currentValue, index, arr) => {
                        const newValue = currentValue[Object.keys(currentValue)[0]].map(value => {
                            return ({...value, attrId: index})
                        });
                        return [...acc, ...newValue];
                    }, [])
                    const modifiedAttrs = chunkArray(newAttrs, 17);
                    return modifiedAttrs.map((mAttr, maIndex) => 
                        `
                            ${maIndex !== 0 ? startView : ''}

                                ${maIndex === 0 ? 
                                    `
                                        ${printDesc(disease)}
                                        <div class='dis-container t-margin'>
                                            <div class='dis-header primary t-bold border-b'>
                                                ${disease.name}
                                            </div>
                                            
                                            ${mAttr.map((attr, aIndex, arr) => 
                                                `
                                                    ${aIndex === 0 || (aIndex > 0 && attr.attrId !== arr[aIndex - 1].attrId) ? 
                                                        `
                                                            <div class='dis-attr border-b'>
                                                                <div class='dis dis-header'>
                                                                    ${Object.keys(disease.attrs.items[attr.attrId])[0]}
                                                                </div>
                                                                <div class='attr-wrapper'>
                                                        ` : ''
                                                    }
                                                    <div class='dis-attr attr-p ${aIndex !== arr.length - 1 && attr.attrId === arr[aIndex + 1].attrId ? 'border-b' : ''}'>
                                                        <div class='attr-name'>
                                                            ${attr.title} 
                                                            ${attr.refs.length !== 0 ? 
                                                                `
                                                                    <div class='links-wrapper'>
                                                                        [${attr.refs.map((ref, index, arr) => 
                                                                            `<a class='ref-link primary t-bold' href='#${ref}'>${ref}</a>${index !== arr.length - 1 ? ', ' : ''}`
                                                                        ).join('')}]
                                                                    </div>
                                                                ` : ''
                                                            }
                                                        </div>
                                                        ${attr.isBoolean !== undefined ? 
                                                            `
                                                                <div class='dis-status-alt'>
                                                                    <i 
                                                                        class="fas fa-minus-circle" 
                                                                        style="
                                                                            margin-right: 20px;
                                                                            color: ${disease.attrs.isPositive === attr.isBoolean ? '#83c341' : '#d0021b'}
                                                                        "
                                                                    ></i>
                                                                    ${attr.isBoolean ? 'var' : 'yok'}
                                                                </div>
                                                            ` : 
                                                            `
                                                                <div class='attr-value'>
                                                                    <div class='rank'>
                                                                        <span style='${attr.rank === 'high' ? `background-color: ${status_colors[attr.rank]}` : ''}'></span>
                                                                        <span style='${attr.rank === 'mid' || attr.rank === 'high' ? `background-color: ${status_colors[attr.rank]}` : ''}'></span>
                                                                        <span style='background-color: ${status_colors[attr.rank]}'></span>
                                                                    </div>
                                                                    ${attr.rankTitle}
                                                                </div>
                                                            `
                                                        }
                                                    </div>
                                                    
                                                    ${aIndex === arr.length - 1 || (aIndex !== arr.length - 1 && attr.attrId !== arr[aIndex + 1].attrId) ? 
                                                        `
                                                                </div>
                                                            </div>
                                                        ` : ''
                                                    }
                                                `
                                            ).join('')}
                                        </div>
                                    ` : 
                                    `
                                        <div class='dis-container t-margin'>
                                            ${mAttr.map((attr, aIndex, arr) => 
                                                `
                                                    ${aIndex === 0 || (aIndex > 0 && attr.attrId !== arr[aIndex - 1].attrId) ? 
                                                        `
                                                            <div class='dis-attr border-b'>
                                                                <div class='dis dis-header'>
                                                                    ${Object.keys(disease.attrs.items[attr.attrId])[0]}
                                                                </div>
                                                                <div class='attr-wrapper'>
                                                        ` : ''
                                                    }
                                                    <div class='dis-attr attr-p ${aIndex !== arr.length - 1 && attr.attrId === arr[aIndex + 1].attrId ? 'border-b' : ''}'>
                                                        <div class='attr-name'>
                                                            ${attr.title} 
                                                            ${attr.refs.length !== 0 ? 
                                                                `   
                                                                    <div class='links-wrapper'>
                                                                        [${attr.refs.map((ref, index, arr) => 
                                                                            `<a class='ref-link primary t-bold' href='#${ref}'>${ref}</a>${index !== arr.length - 1 ? ', ' : ''}`
                                                                        ).join('')}]
                                                                    </div>
                                                                ` : ''
                                                            }
                                                        </div>
                                                        ${attr.isBoolean !== undefined ? 
                                                            `
                                                                <div class='dis-status-alt'>
                                                                    <i 
                                                                        class="fas fa-minus-circle" 
                                                                        style="
                                                                            margin-right: 20px;
                                                                            color: ${disease.attrs.isPositive === attr.isBoolean ? '#83c341' : '#d0021b'}
                                                                        "
                                                                    ></i>
                                                                    ${attr.isBoolean ? 'var' : 'yok'}
                                                                </div>
                                                            ` : 
                                                            `
                                                                <div class='attr-value'>
                                                                    <div class='rank'>
                                                                        <span style='${attr.rank === 'high' ? `background-color: ${status_colors[attr.rank]}` : ''}'></span>
                                                                        <span style='${attr.rank === 'mid' || attr.rank === 'high' ? `background-color: ${status_colors[attr.rank]}` : ''}'></span>
                                                                        <span style='background-color: ${status_colors[attr.rank]}'></span>
                                                                    </div>
                                                                    ${attr.rankTitle}
                                                                </div>
                                                            `
                                                        }
                                                    </div>
                                                    
                                                    ${aIndex === arr.length - 1 ? 
                                                        `
                                                                </div>
                                                            </div>
                                                        ` : ''
                                                    }
                                                `
                                            ).join('')}
                                        </div>
                                    `
                                }
                                
                            ${endView}
                        `
                    ).join('');
                }
                
                return diseases.map((disease, index, arr) => {
                    let pageView = '';
                    const diseaseView = printDisease(disease);
                    const height = getContentHeight(diseaseView) + 20;
                    heightSum += height;

                    if (index === 0) pageView = startView;

                    if(heightSum <= totalHeight){
                        pageView += diseaseView;
                        if (index === arr.length - 1) pageView += endView
                        console.log(pageView);

                        return pageView;
                    }else {
                        if(height > totalHeight){
                            heightSum = 0;
                            let diseaseAltView = '';
                            if(disease.desc.length >= 4){
                                diseaseAltView = printDesc(disease) + endView + startView + printDiseaseTable(disease) + endView;
                            }else {
                                if(!disease.attrs.is_array){
                                    diseaseAltView = printNormalDisease(disease);
                                }else {
                                    diseaseAltView = printComplexDisease(disease);
                                }
                            }
                            
                            if(index === 0){
                                return pageView += diseaseAltView + startView
                            }

                            const oldView = printDisease(arr[index - 1]);
                            const oldHeight = getContentHeight(oldView);
                            pageView = oldHeight <= totalHeight ? endView + startView + diseaseAltView : diseaseAltView;

                            return index !== arr.length - 1 ? pageView + startView : pageView
                        }
                        heightSum = height;
                        pageView += endView + startView + diseaseView;
                        if (index === arr.length - 1) pageView += endView
                        return pageView;
                    }
                }).join('');
            }, trans, isSecret, kitCode, registerDate, diseases, status_colors);
        }catch(err){
            console.log('Test err: ', err);
            return;
        }
    })()
})

hbs.registerHelper('printNutrients', (req, isSecret, kitCode, registerDate, nutrients) => {
    const modifiedNutrients = chunkArray(nutrients, 2);
    return modifiedNutrients.map((nutrient, index) => 
        `
            <div id='nutrient-page-${index}' class='page portrait'>
                <div class='page-header'>
                    <div class='report-name'>
                        <label class='t-bold secondary'>${req.__("p-head-l-0")}</label>
                        <div>${req.__("m-report-title")}</div>
                    </div>
                    <div class='report-kitCode'>
                        <label class='t-bold secondary'>${req.__("p-head-l-1")}</label>
                        <div>
                            ${isSecret ? '.....' : `${kitCode}`}
                        </div>
                    </div>
                    <div>
                        <label class='t-bold secondary'>${req.__("p-head-l-2")}</label>
                        <div>${registerDate}</div>
                    </div>
                </div>
                <hr class='line-breaker'>

                ${index === 0 ? 
                    `
                        <div class='main-title t-bold primary'>
                            ${req.__("nS-title")}
                        </div>
                        <div class='t-margin'>
                            ${req.__('fm-desc-1')}
                        </div>
                        <div class='t-margin'>
                            <div>
                                ${req.__('fm-item-0')}
                            </div>
                            <div>
                                ${req.__('fm-item-1')}
                            </div>
                            <div>
                                ${req.__('fm-item-2')}
                            </div>
                        </div>
                    ` : 
                    ''
                }
                ${nutrient.map(nut => 
                    `
                        <div class='alt-title t-margin t-bold primary'>
                            ${nut.title}
                        </div>
                        <div class='f-card-container'>
                            ${nut.foods.map(food => 
                                `
                                    <div class='f-card'>
                                        <div class='t-capita t-bold f-name legendary'>
                                            ${food.name}
                                        </div>
                                        <div class='status'>
                                            <div class='rank'>
                                                <span style='${food.rank === 'iyi' ? `background-color: ${colors[food.rank]}` : ''}'></span>
                                                <span style='${food.rank === 'orta' || food.rank === 'iyi' ? `background-color: ${colors[food.rank]}` : ''}'></span>
                                                <span style='background-color: ${colors[food.rank]}'></span>
                                            </div>
                                            <div class='f-value'>
                                                ${food.value}
                                            </div>
                                        </div>
                                    </div>
                                `
                            ).join('')}
                        </div>
                    `
                ).join('')}

                <div class='page-footer'>
                    <img class='footer-img' src='https://enbiosis-pdf-generator.herokuapp.com/img/logo-2x.png' alt='enbiosis-logo'/>
                </div>
            </div>
        `
    ).join('');
})

hbs.registerHelper('printReferences', (req, isSecret, kitCode, registerDate, references) => {
    const initialRefs = references.slice(0, 24);
    const firstPage = `
        <div id='reference-page' class='page portrait'>
            <div class='page-header'>
                <div class='report-name'>
                    <label class='t-bold secondary'>${req.__("p-head-l-0")}</label>
                    <div>${req.__("m-report-title")}</div>
                </div>
                <div class='report-kitCode'>
                    <label class='t-bold secondary'>${req.__("p-head-l-1")}</label>
                    <div>
                        ${isSecret ? '.....' : `${kitCode}`}
                    </div>
                </div>
                <div>
                    <label class='t-bold secondary'>${req.__("p-head-l-2")}</label>
                    <div>${registerDate}</div>
                </div>
            </div>
            <hr class='line-breaker'>

            <div class='main-title t-bold t-margin primary'>
                ${req.__("references")}
            </div>
            <div class='ref-wrapper' style='height: calc(1100px - 59px)'>
                ${initialRefs.map(ref => 
                    `
                        <div id='${ref.id}' class='ref-container'>
                            <div class='ref-num'>
                                [${ref.id}]
                            </div>
                            <div>
                                ${ref.title}.
                            </div>
                        </div>
                    `
                ).join('')}
            </div>

            <div class='page-footer'>
                <img class='footer-img' src='https://enbiosis-pdf-generator.herokuapp.com/img/logo-2x.png' alt='enbiosis-logo'/>
            </div>
        </div>
    `
    const modifiedRefs = chunkArray(references.slice(24), 23);
    const viewList = modifiedRefs.map((refItems, index) => 
        `
            <div id='reference-page-${index}' class='page portrait'>
                <div class='page-header'>
                    <div class='report-name'>
                        <label class='t-bold secondary'>${req.__("p-head-l-0")}</label>
                        <div>${req.__("m-report-title")}</div>
                    </div>
                    <div class='report-kitCode'>
                        <label class='t-bold secondary'>${req.__("p-head-l-1")}</label>
                        <div>
                            ${isSecret ? '.....' : `${kitCode}`}
                        </div>
                    </div>
                    <div>
                        <label class='t-bold secondary'>${req.__("p-head-l-2")}</label>
                        <div>${registerDate}</div>
                    </div>
                </div>
                <hr class='line-breaker'>

                <div class='ref-wrapper'>
                    ${refItems.map(ref => 
                        `
                            <div id='${ref.id}' class='ref-container'>
                                <div class='ref-num'>
                                    [${ref.id}]
                                </div>
                                <div>
                                    ${ref.title}.
                                </div>
                            </div>
                        `
                    ).join('')}
                </div>

                <div class='page-footer'>
                    <img class='footer-img' src='https://enbiosis-pdf-generator.herokuapp.com/img/logo-2x.png' alt='enbiosis-logo'/>
                </div>
            </div>
        `
    ).join('');

    return `${firstPage}${viewList}`;
})


// PROGRAM PDF HELPERS
hbs.registerHelper('printNutritionProgram', (browser, req, kitCode, registerDate, content, start_date, end_date) => {
    return (async () => {
        try {
            const testPage = await browser.newPage();
            const testContent = await this.compile('test', {
                req, 
                kitCode, 
                registerDate
            });
            await testPage.setContent(testContent, {waitUntil: 'load'})

            await testPage.evaluate(() => {
                window.getContentHeight = view => {
                    const viewHtml = document.querySelector('#content');
                    viewHtml.innerHTML = view;
                    const height = viewHtml.clientHeight;
                    viewHtml.innerHTML = '';
                    return height + 20;
                }
            })
            /***************************************/

            const trans = {
                report: req.__('p-head-l-0'),
                report_title: req.__('nP-title'),
                user: req.__('p-head-l-1'),
                date: req.__('p-head-l-2'),
                breakfast: req.__('breakfast'),
                lunch: req.__('lunch'),
                dinner: req.__("dinner"),
                snack: req.__("snack"),
                note: req.__("note")
            }

            return await testPage.evaluate((trans, kitCode, registerDate, content, start_date, end_date) => {
                const totalHeight = 1100;
                let heightSum = 0;
                const startView = `
                    <div class='page portrait'>
                        <div class='page-header'>
                            <div class='report-name'>
                                <label class='t-bold secondary'>${trans.report}</label>
                                <div>${trans.report_title}</div>
                            </div>
                            <div class='report-kitCode'>
                                <label class='t-bold secondary'>${trans.user}</label>
                                <div>
                                    ${kitCode}
                                </div>
                            </div>
                            <div>
                                <label class='t-bold secondary'>${trans.date}</label>
                                <div>${registerDate}</div>
                            </div>
                            <div class='dates-container-header'>
                                <div>
                                    <i class="far fa-calendar-alt pC-icon-header"></i>
                                    <span>${start_date}</span> - <span>${end_date}</span>
                                </div>
                            </div>
                        </div>
                        <hr class='line-breaker'>
                `;
                const endView = `
                        <div class='page-footer'>
                            <img class='footer-img' src='https://enbiosis-pdf-generator.herokuapp.com/img/logo-2x.png' alt='enbiosis-logo'/>
                        </div>
                    </div>
                `;

                const printContent = con => (`
                    <div class='t-margin'>
                        <div class='con-type primary t-bold'>
                            <img class='meal-img' src='https://enbiosis-pdf-generator.herokuapp.com/img/meals-img/${con.type}.png'/>
                            ${trans[con.type]}
                        </div>
                        <div class='con-details'>${con.details}</div>
                    </div>
                `);

                return content.map((con, index, arr) => {
                    let pageView = '';
                    const conView = printContent(con);
                    const height = getContentHeight(conView);
                    heightSum += height;
                    // console.log(height);
                    // console.log(con);
                    // console.log('***************************************');

                    if (index === 0) pageView = startView;

                    if(heightSum <= totalHeight){
                        pageView += conView;
                        if(index === arr.length - 1) pageView += endView
                        return pageView
                    }else if(heightSum > totalHeight){
                        heightSum = height;
                        pageView+= endView + startView + conView;
                        if (index === arr.length - 1) pageView += endView
                        return pageView
                    }
                }).join('');
            }, trans, kitCode, registerDate, content, start_date, end_date)
        }catch(err){
            console.log('Test err: ', err);
            return;
        }
    })()
})



// for test reasons
// hbs.registerHelper('printTest', (browser, req, kitCode, registerDate, allFoods) => {
//     (async () => {
//         const testPage = await browser.newPage();
//         await testPage.setViewport({ width: 1920, height: 1400 });
//         const testContent = await this.compile('test', {
//             req, 
//             kitCode, 
//             registerDate, 
//             allFoods
//         });
//         await testPage.setContent(testContent, {waitUntil: 'load'})
//         await testPage.screenshot({
//             path: './arFood.jpg',
//             type: 'jpeg',
//             clip: { x: 70, y: 70, width: 500, height: 820 }
//         })
//         await testPage.close();
//     })()
// })
// hbs.registerHelper('printSomeFood', () => 
//     `
//         <div class='food-container' dir='rtl'>
//             ${arFoods.map(food => 
//                 `
//                     <div class='food-item-card'>
//                         <img class='f-img' src='${food.imageUrl}' title='${food.altTitle}'/>
//                         <div class='t-capita t-bold legendary'>
//                             ${food.title}
//                         </div>
//                         <svg 
//                             id="Layer_1" 
//                             data-name="Layer 1" 
//                             xmlns="http://www.w3.org/2000/svg" 
//                             width='40' height='40'
//                             viewBox="0 0 288.38 288.38"
//                         >
//                             ${getRankSvg(food.rank)}
//                             <text 
//                                 style='font-size: 8rem;'
//                                 x='50%' 
//                                 y='65%' 
//                                 text-anchor='middle'
//                                 fill='white'
//                             >
//                                 ${food.value}
//                             </text>
//                         </svg>
//                     </div>
//                 `
//             ).join('')}
//         </div>
//     `
// )

// const arFoods = [
//     {
//         title: 'بذور الكتان',
//         altTitle: 'keten-tohumu',
//         value: '8',
//         rank: 'iyi',
//         imageUrl: 'https://www.enbiosis.com/foods/Ya%C4%9Flar/Keten%20Tohumu.jpg'
//     },
//     {
//         title: 'لوز',
//         altTitle: 'badem',
//         value: '10',
//         rank: 'iyi',
//         imageUrl: 'https://www.enbiosis.com/foods/Yağlar/Badem.jpg'
//     },
//     {
//         title: 'توت العليق',
//         altTitle: 'ahududu',
//         value: '5',
//         rank: 'orta',
//         imageUrl: 'https://www.enbiosis.com/foods/Meyveler/Ahududu.jpg'
//     },
//     {
//         title: 'جمبري',
//         altTitle: 'karides',
//         value: '4',
//         rank: 'orta',
//         imageUrl: 'https://www.enbiosis.com/foods/Et%20ve%20Et%20%C3%9Cr%C3%BCnleri/Karides.jpg'
//     },
//     {
//         title: 'فطر',
//         altTitle: 'mantar',
//         value: '6',
//         rank: 'orta',
//         imageUrl: 'https://www.enbiosis.com/foods/Sebzeler/Mantar.jpg'
//     },
//     {
//         title: 'إجاص',
//         altTitle: 'armut',
//         value: '4',
//         rank: 'orta',
//         imageUrl: 'https://www.enbiosis.com/foods/Meyveler/Armut.jpg'
//     },
//     {
//         title: 'جوز',
//         altTitle: 'ceviz',
//         value: '7',
//         rank: 'orta',
//         imageUrl: 'https://www.enbiosis.com/foods/Yağlar/Ceviz.jpg'
//     },
//     {
//         title: 'أناناس',
//         altTitle: 'ananas',
//         value: '3',
//         rank: 'kotu',
//         imageUrl: 'https://www.enbiosis.com/foods/Meyveler/Ananas.jpg'
//     },
//     {
//         title: 'سمك السلمون',
//         altTitle: 'somon',
//         value: '1',
//         rank: 'kotu',
//         imageUrl: 'https://www.enbiosis.com/foods/Et%20ve%20Et%20%C3%9Cr%C3%BCnleri/Somon.jpg'
//     }
// ]